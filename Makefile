#
# @author: Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
# @since: Aug 27, 2020
#

.PHONY: all clean install configure configure_debug docs

all:
	$(MAKE) -C build

clean:
	$(MAKE) -C build clean

install:
	$(MAKE) -C build install

configure:
	@rm -rf build; mkdir build; cd build; cmake -USE_PYTHON2=OFF -DCMAKE_BUILD_TYPE=Release ..; cd ..
	@ln -fs build/compile_commands.json compile_commands.json

configure_debug:
	@rm -rf build; mkdir build; cd build; cmake -USE_PYTHON2=OFF -DCMAKE_BUILD_TYPE=Debug ..; cd ..
	@ln -fs build/compile_commands.json compile_commands.json

docs:
	doxygen doxygen.cfg
