import abaq
import collections

from markets import *

# Create connection to the database.
DB = abaq.external.mysql("<connection string>")

BD = BrokerDealter()  # Create BrokerDealer object from SO definitions.

U = abaq.Dimension()  # Declare Universe dimension

Trade = collections.namedtuple('Trade', ['qty', 'price', 'timestamp'])
Quote = collections.namedtuple('Quote', ['spd', 'yld', 'price', 'penalty'])

Universe = CorpBond(DB.query(U, "SELECT security FROM CoverageUniverse WHERE type = 'CorpBond'"))

Trades = Trade(*DB.query(T, "SELECT qty, price, timestamp, security FROM Trades ORDER BY timestamp", Universe.id))

# Create a Portfolio with timeseries contents simulated over time. Every new timed content slice
# is defined by the recurrence function `sim` based on previous timed content slice.
Port = Portfolio(contents=abaq.phi(T, 0, lambda i, j: sim(i, j)))


def checkBoundaryLimits(Qty):
    I = abaq.Dimension()
    Utils = abaq.pack(I, [rl.util(Qty, Port) for rl in BD.boundaryRiskLimits()])

    return (Utils.utilAfter.abs() < 1.0 or Utils.utilAfter.abs() < Utils.utilBefore.abs()).reduce('and', I)


def checkQuoteOut(Qty, threshold):
    I = abaq.Dimension()
    Driver = abaq.pack(I, [rl.driverEval(Qty, Port) for rl in BD.riskLimits()])

    return (Driver.driverBefore.abs() >= threshold).reduce('or', I)


def calcQuote(Qty, Penalty):
    Skew = querySkewModel(Qty)
    Bias = queryBiasModel(Qty)

    Spread = Universe.baseBidSpd - Universe.baseAskSpd
    BaseBidSpdAdj = Universe.baseBidSpd - (Bias + Skew) * Spread
    BaseAskSpdAdj = Universe.baseAskSpd - (Bias - Skew) * Spread

    SpreadAdj = BaseBidSpdAdj - BaseAskSpdAdj
    SpreadQuoted = SpreadAdj * (1 - 2 * Penalty)
    Adj = (SpreadAdj - SpreadQuoted) / 2

    return (SpreadAdj > 0).iif((Qty > 0).iif(BaseBidSpdAdj - Adj, BaseAskSpdAdj + Adj), None)


def calcYieldPrice(BenchmarkYield, Spread):
    Yield = BenchmarkYield + Spread / 100.0
    Price = Universe.cleanPrice(Yield)
    return (Yield, Price)


def quoteBid(Qty):
    # age-driven position penalty skew
    PenaltyPos = BD.positionPenaltySkew(Qty, Port)
    SpreadPos = calcQuote(Qty, PenaltyPos)
    YieldPos, PricePos = calcYieldPrice(Universe.benchmark.bidYld, SpreadPos)
    QuotePos=Q(spd=SpreadPos, yld=YieldPos, price=PricePos, penalty=PenaltyPos)

    # quote out logic
    PenaltyOut = penaltyOut(Qty)
    SpreadOut = calcQuote(Qty, PenaltyOut)
    YieldOut, PriceOut = calcYeldPrice(Universe.benchmark.bidYld, SpreadOut)
    QuoteOut = Q(spd=SpreadOut, yld=YieldOut, price=PriceOut, penalty=PenaltyOut)

    # quote in logic
    SpreadIn = calcQuote(Qty, 0)
    YieldIn, PriceIn = calcYieldPrice(Universe.benchmark.bidYld, SpreadIn)
    QuoteIn = Q(spd = SpreadIn, yld = YieldIn, price = PriceIn, penalty = 0)

    # check if the position penalty skew
    return ((Port.pos() + Qty).abs() < Port.pos().abs()).iif(QuotePos, checkQuoteOut(Qty, cs01Threshold).iif(QuoteOut, QuoteIn))


def quoteAsk(Qty):
    # similar to `quoteBid()`
    pass


def quote(Qty):
    return (Qty > 0).iif(quoteBid(Qty), quoteAsk(Qty))


def sim(i, j):
    Quote = quote(Trades.qty(i))
    Won = (Trades.qty(i) > 0).iif(Quote.price > Trades.price(i), Quote.price < Trades.price(i))

    return Won.iif(P(qty = Trades.qty(i), price = Quote.price), P())

