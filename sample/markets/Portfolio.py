import abaq

class Portfolio(abaq.Object):

    pos = abaq.Attr(expr = lambda self: abaq.phi(T, 0, lambda i, j: self.pos(j) + self.contents(i).qty))

    age = abaq.Attr(expr = lambda self: abaq.phi(T, 0, lambda i, j: self.age_(i, j)


    def __init__(self, contents, time):
        self.contents = contents.claim(P)
        self.time = time.claim(T)


    def age_(self, i, j):
        return (self.age(j) + abaq.time.busDaysDiff(self.time(i), self.time(j)) * self.pos(j) + self.contents(i).qty) / (self.pos(j) + self.contents(i).qty)
