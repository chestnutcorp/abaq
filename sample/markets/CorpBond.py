import abaq

class CorpBond(abaq.Object):


    def __init__(self, ids):
        self.prefix_, self.suffix_ = ids.regex("(\\w{8,9}(?:\\w{3})?)(?:@\\w{4})?( Corp)")

        self.baseBidSpd, self.baseAskSpd = abaq.dynamic('cpp.bid.spread, cpp.ask.spread FROM <data provider connection string>', id())


    def id(qualifier = ''):
        return self.prefix_ + qualifier + self.suffix_
