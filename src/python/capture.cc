/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com
 * \since Sep 1, 2020
 */
#include "binder.h"
#include "capture.h"
#include "engine.h"
#include "tensor.h"

#include "abaq/graph/functor.h"

#include <exception>
#include <stdexcept>

using namespace abaq;


static PyMethodDef methods[] = {
	{
		"eval", python::capture::eval, METH_VARARGS,
		"Evaluates captured tensors associated with the capture."
	},
	{
		"data", python::capture::data, METH_VARARGS,
		"Returns tensor data as NumPy array."
	},
	{ nullptr, nullptr, 0, nullptr }
};

PyTypeObject python::capture::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.Capture",
	.tp_doc = "Session controls captured tensors evaluation.",
	.tp_basicsize = sizeof(python::capture),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_dealloc = python::capture::dealloc,
	.tp_methods = methods,
	.tp_new = python::capture::create,
};

bool python::capture::register_type(PyObject * m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, "Capture", (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

python::capture * python::capture::cast(PyObject * self)
{
	return Py_TYPE(self) == &type ? static_cast<capture *>(self) : nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void python::capture::dealloc(PyObject * self)
{
	auto obj = static_cast<capture *>(self);
	obj->capture_.~capture();

	Py_TYPE(self)->tp_free(self);
}

PyObject * python::capture::create(PyTypeObject * subtype, PyObject * args, PyObject * kwds)
{
	(void) kwds;

	try
	{
		if (PyTuple_Size(args) < 2)
			throw std::runtime_error("Too few arguments.");

		PyObject * arg = PyTuple_GetItem(args, 0);
		if (PyObject_IsInstance(arg, reinterpret_cast<PyObject *>(&engine::type)))
		{
			auto e = engine::cast(arg)->engine_;

			graph::inputs<void> inputs;

			for (Py_ssize_t index = 1, count = PyTuple_Size(args); index < count; ++index)
			{
				arg = PyTuple_GetItem(args, index);

				if (PyObject_IsInstance(arg, reinterpret_cast<PyObject *>(&tensor::type)))
					inputs.push_back(tensor::convert(arg, true));
				else
					throw std::runtime_error("Unexpected argument type. An abaq.Tensor expected.");
			}


			auto obj = static_cast<capture *>(subtype->tp_alloc(subtype, 0));
			if (obj)
			{
				new (&obj->capture_) graph::capture(e, inputs);
			}

			return obj;
		}
		else
			throw std::runtime_error("First argument must be an abaq.Engine reference.");
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
	}

	return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject * python::capture::eval(PyObject * self, PyObject * args)
{
	(void) args;

	try
	{
		auto obj = cast(self);
		if (obj)
		{
			std::list<binder> binders;
			binder::map map;

			if (PyTuple_Size(args) > 0)
			{
				PyObject * dict = PyTuple_GetItem(args, 0);
				if (PyDict_Check(dict))
				{
					PyObject * key, * value;
					Py_ssize_t pos = 0;

					while (PyDict_Next(dict, &pos, &key, &value))
					{
						auto t = tensor::cast<void>(key);

						binders.emplace_back(value);
						map.emplace(obj->capture_.get_index(t), &binders.back());
					}
				}
				else
					throw std::runtime_error("Dict expected");
			}

			obj->capture_.eval(map);

			Py_RETURN_NONE;
		}
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
	}

	return nullptr;
}

PyObject * python::capture::data(PyObject * self, PyObject * args)
{
	try
	{
		tensor * t;

		if (PyArg_ParseTuple(args, "O!", &tensor::type, &t))
		{
			auto obj = cast(self);
			if (obj && t->tensor_.type() == value_type::number)
			{
				auto buf = obj->capture_.get_membuf(t->tensor_);
				if (buf)
				{
					std::vector<npy_intp> dims;
					std::for_each(buf->dimensions().begin(), buf->dimensions().end(), [&dims](auto const &d) { dims.push_back(d.size()); });

					return PyArray_SimpleNewFromData(dims.size(), dims.data(), NPY_DOUBLE, reinterpret_cast<void *>(buf->num_data()));
				}

			}
			else
				Py_RETURN_NONE;
		}
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
	}

	return nullptr;
}

