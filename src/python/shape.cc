/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com
 * \since Jul 10, 2020
 */

#include "shape.h"

#include <exception>
#include <sstream>

using namespace abaq;


static PyMethodDef methods[] = {
	{
		"rank", python::shape::rank, METH_NOARGS,
		"Returns shape rank (number of dimensions)."
	},
	{
		"volume", python::shape::volume, METH_NOARGS,
		"Returns shape volume"
	},
	{ nullptr, nullptr, 0, nullptr }
};

static PyNumberMethods number_methods = {
	.nb_add = python::shape::add,
	.nb_subtract = python::shape::sub,
	.nb_multiply = python::shape::mul,
};

PyTypeObject python::shape::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.Shape",
	.tp_doc = "Shape represents a set of dimensions.",
	.tp_basicsize = sizeof(python::shape),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_dealloc = python::shape::dealloc,
	.tp_repr = python::shape::repr,
	.tp_as_number = &number_methods,
	.tp_richcompare = python::shape::compare,
	.tp_methods = methods,
};

bool python::shape::register_type(PyObject *m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, "Shape", (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

python::shape *python::shape::cast(PyObject *self)
{
	return Py_TYPE(self) == &type ? static_cast<shape*>(self) : nullptr;
}

PyObject *python::shape::wrap(const graph::shape &s)
{
	auto obj = static_cast<shape *>(type.tp_alloc(&type, 0));
	if (obj)
	{
		new (&obj->shape_) graph::shape(s);
	}

	return obj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void python::shape::dealloc(PyObject *self)
{
	auto obj = static_cast<shape *>(self);
	obj->shape_.~shape();

	Py_TYPE(self)->tp_free(self);
}

PyObject *python::shape::repr(PyObject *self)
{
	auto obj = cast(self);
	if (obj)
	{
		std::ostringstream buf;
		buf << "<shape";
		std::for_each(obj->shape_.begin(), obj->shape_.end(), [&buf](const auto &d) { buf << ' ' << d; });
		buf << '>';
		
		auto repr = buf.str();
		return PyUnicode_FromStringAndSize(repr.data(), repr.size());
	}
	else
		return nullptr;
}

PyObject *python::shape::compare(PyObject *obj1, PyObject *obj2, int op)
{
	try
	{
		bool res = false;

		shape *s1 = cast(obj1);
		shape *s2 = cast(obj2);

		if (s1 && s2)
		{
			switch (op)
			{
				case Py_EQ: res = s1->shape_ == s2->shape_; break;
				case Py_NE: res = s1->shape_ != s2->shape_; break;
			}
		}

		PyObject *result = res ? Py_True : Py_False;
		Py_INCREF(result);
		return result;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject *python::shape::rank(PyObject *self, PyObject *args)
{
	(void) args;

	try
	{
		shape *s = cast(self);
		if (s)
			return PyLong_FromSize_t(s->shape_.rank());
		else
			return nullptr;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject *python::shape::volume(PyObject *self, PyObject *args)
{
	(void) args;

	try
	{
		shape *s = cast(self);
		if (s)
			return PyLong_FromSize_t(s->shape_.volume());
		else
			return nullptr;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject *python::shape::add(PyObject *obj1, PyObject *obj2)
{
	try
	{
		shape* s1 = cast(obj1);
		shape* s2 = cast(obj2);

		if (s1 && s2)
		{
			return wrap(s1->shape_ + s2->shape_);
		}
		else
		{
			PyErr_BadArgument();
			return nullptr;
		}
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject *python::shape::sub(PyObject *obj1, PyObject *obj2)
{
	try
	{
		shape *s1 = cast(obj1);
		shape *s2 = cast(obj2);

		if (s1 && s2)
		{
			return wrap(s1->shape_ - s2->shape_);
		}
		else
		{
			PyErr_BadArgument();
			return nullptr;
		}
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject *python::shape::mul(PyObject *obj1, PyObject *obj2)
{
	try
	{
		shape *s1 = cast(obj1);
		shape *s2 = cast(obj2);

		if (s1 && s2)
		{
			return wrap(s1->shape_ * s2->shape_);
		}
		else
		{
			PyErr_BadArgument();
			return nullptr;
		}
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

