/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Sep 1, 2020
 */
#pragma once

#include "abaq/graph/capture.h"

#include <Python.h>

#define PY_ARRAY_UNIQUE_SYMBOL ABAQ_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#ifndef ABAQ_IMPORT_ARRAY
#define NO_IMPORT_ARRAY
#endif

#include <numpy/arrayobject.h>


namespace abaq
{
	namespace python
	{
		struct capture: public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject *m);


			graph::capture capture_;


			static capture * cast(PyObject * self);

			// support
			static void dealloc(PyObject * self);
			static PyObject * create(PyTypeObject * subtype, PyObject * args, PyObject * kwds);

			// methods
			static PyObject * eval(PyObject * self, PyObject * args);
			static PyObject * data(PyObject * self, PyObject * args);
		};
	}
}
