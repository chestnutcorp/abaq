/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */
#pragma once

#include <Python.h>

#include "abaq/graph/factory.h"
#include "abaq/graph/functor.h"

namespace abaq
{
	namespace python
	{
		PyObject *wrap(graph::functor_base *f);

		template <typename F, typename ...Args> PyObject *create_functor(Args... args)
		{
			return wrap(graph::factory::get_default_factory()->create<F>(args...));
		}
	}
}
