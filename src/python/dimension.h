/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since May 11, 2021
 */
#pragma once

#include "abaq/graph/dimension.h"

#include <Python.h>

namespace abaq
{
	namespace python
	{
		struct dimension : public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject *m);


			graph::dimension dimension_;


			static dimension *cast(PyObject *self);
			static PyObject *wrap(const graph::dimension &d);

			// support
			static PyObject *create(PyTypeObject *subtype, PyObject *args, PyObject *kwds);
			static void dealloc(PyObject *self);
			static PyObject *repr(PyObject *self);
			static PyObject *compare(PyObject *obj1, PyObject *obj2, int op);

			// methods
			static PyObject *size(PyObject *self, PyObject *args);
		};
	}
}
