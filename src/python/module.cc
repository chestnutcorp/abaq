/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 7, 2020
 */
#define ABAQ_IMPORT_ARRAY
#include "module.h"

#include "attribute.h"
#include "capture.h"
#include "dimension.h"
#include "engine.h"
#include "shape.h"
#include "tensor.h"

#include "abaq/functors/core.h"
#include "abaq/functors/number.h"
#include "abaq/functors/string.h"

using namespace abaq;


static PyMethodDef abaq_methods[] = {
	{
		"number", python::create_number, METH_VARARGS,
		"Creates numeric tensor of specified shape."
	},
	{
		"string", python::create_string, METH_VARARGS,
		"Creates string tensor of specified shape."
	},
	/*{
		"serial", python::create_serial, METH_VARARGS,
		"Creates serial tensor representing current date/time."
	},*/
	{
		"pack", python::create_pack, METH_VARARGS,
		"Packs tensors into a tensor of a higher rank."
	},
	{
		"list", python::create_list, METH_VARARGS,
		"Creates tensor of listed numeric/string items."
	},
	{
		"range", python::create_range, METH_VARARGS,
		"Creates numeric tensor for specified range."
	},
	{ nullptr, nullptr, 0, nullptr }
};

static struct PyModuleDef abaq_module = {
	PyModuleDef_HEAD_INIT,
	.m_name = ABAQ_MODULE,
	.m_doc = "High performance computing platform",
	.m_size = -1,
	.m_methods = abaq_methods,
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject * python::create_number(PyObject * self, PyObject * args)
{
	(void) self;

	try
	{
		shape * s;
		number v;

		if (PyArg_ParseTuple(args, "O!d", &shape::type, &s, &v))
		{
			return create_functor<functors::immediate<number>>(v, s->shape_);
		}
		else
		{
			PyErr_Clear();

			graph::dimension::list dims;

			for (Py_ssize_t index = 0, count = PyTuple_GET_SIZE(args); index < count; ++index)
			{
				auto d = dimension::cast(PyTuple_GET_ITEM(args, index));
				if (!d) throw std::invalid_argument("Dimension expected.");

				dims.push_back(d->dimension_);
			}

			return create_functor<functors::bind<number>>(dims);
		}
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::create_string(PyObject * self, PyObject * args)
{
	(void) self;

	try
	{
		shape * s;
		char const * v;

		if (PyArg_ParseTuple(args, "O!s", &shape::type, &s, &v))
		{
			return create_functor<functors::immediate<string>>(v, s->shape_);
		}
		else
		{
			graph::dimension::list dims;

			for (Py_ssize_t index = 0, count = PyTuple_GET_SIZE(args); index < count; ++index)
			{
				auto d = dimension::cast(PyTuple_GET_ITEM(args, index));
				if (!d) throw std::invalid_argument("Dimension expected.");

				dims.push_back(d->dimension_);
			}

			return create_functor<functors::bind<string>>(dims);
		}
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

/*
PyObject * python::create_serial(PyObject * self, PyObject * args)
{
	try
	{
		assert(PyObject_TypeCheck(self, &type));
		auto w = static_cast<warehouse *>(self);

		shape *s;

		if (PyArg_ParseTuple(args, "O!", &shape::type, &s))
		{
			return w->create_functor<functors::serial>(*s->shape_);
		}
		else
			return nullptr;
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}
*/

PyObject * python::create_pack(PyObject * self, PyObject * args)
{
	(void) self;

	try
	{
		auto pack_functor = [](dimension * d, Py_ssize_t start, Py_ssize_t end, std::function<PyObject * (Py_ssize_t index)> getter) -> PyObject * {
			auto first = tensor::cast<void>(getter(start)); // get first item to check its type

			if (first.type() == value_type::number)
			{
				graph::inputs<number> inputs;
				inputs.emplace_back(first);

				for (Py_ssize_t index = start + 1; index < end; ++index)
					inputs.emplace_back(tensor::cast<number>(getter(index)));

				return create_functor<functors::pack<number>>(d->dimension_, inputs);
			}

			if (first.type() == value_type::string)
			{
				graph::inputs<string> inputs;
				inputs.emplace_back(first);

				for (Py_ssize_t index = start + 1; index < end; ++index)
					inputs.emplace_back(tensor::cast<string>(getter(index)));

				return create_functor<functors::pack<string>>(d->dimension_, inputs);
			}

			throw std::invalid_argument("Unexpected input type.");
		};

		if (PyTuple_GET_SIZE(args) > 0)
		{
			auto d = dimension::cast(PyTuple_GET_ITEM(args, 0));
			if (d)
			{
				if (PyTuple_GET_SIZE(args) == 2 && PyList_Check(PyTuple_GET_ITEM(args, 1)))
				{
					PyObject *list = PyTuple_GET_ITEM(args, 1);
					if (PyList_GET_SIZE(list) > 0)
						return pack_functor(d, 0, PyList_GET_SIZE(list), [list](Py_ssize_t index) -> PyObject * { return PyList_GET_ITEM(list, index); });
				}
				else
				{
					if (PyTuple_GET_SIZE(args) > 1)
						return pack_functor(d, 1, PyTuple_GET_SIZE(args), [args](Py_ssize_t index) -> PyObject * { return PyTuple_GET_ITEM(args, index); });
				}

				throw std::invalid_argument("No inputs provided");
			}
		}

		throw std::invalid_argument("Dimension expected.");
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::create_list(PyObject * self, PyObject * args)
{
	(void) self;

	try
	{
		auto create_list_functor = [](dimension * d, Py_ssize_t start, Py_ssize_t end, std::function<PyObject * (Py_ssize_t index)> getter) -> PyObject * {
			auto test = getter(start); // get first item to check its type

			if (PyFloat_Check(test) || PyLong_Check(test))
			{
				std::vector<number> items;

				for (Py_ssize_t index = start; index < end; ++index)
				{
					PyObject *item = getter(index);

					if (PyFloat_Check(item))
						items.push_back(PyFloat_AsDouble(item));
					else if (PyLong_Check(item))
						items.push_back(PyLong_AsDouble(item));
					else
						throw std::invalid_argument("Numeric value expected.");
				}

				return create_functor<functors::list<number>>(d->dimension_, items);
			}

			if (PyUnicode_Check(test))
			{
				std::vector<string> items;

				for (Py_ssize_t index = start; index < end; ++index)
				{
					PyObject *item = getter(index);

					if (PyUnicode_Check(item))
						items.emplace_back(PyUnicode_AsUTF8(item));
					else
						throw std::invalid_argument("String value expected.");
				}

				return create_functor<functors::list<string>>(d->dimension_, items);
			}

			throw std::invalid_argument("Numeric and string values are supported only.");
		};

		if (PyTuple_GET_SIZE(args) > 0)
		{
			auto d = dimension::cast(PyTuple_GET_ITEM(args, 0));
			if (d)
			{
				if (PyTuple_GET_SIZE(args) == 2 && PyList_Check(PyTuple_GET_ITEM(args, 1)))
				{
					PyObject *list = PyTuple_GET_ITEM(args, 1);
					if (PyList_GET_SIZE(list) > 0)
						return create_list_functor(d, 0, PyList_GET_SIZE(list), [list](Py_ssize_t index) -> PyObject * { return PyList_GET_ITEM(list, index); });
				}
				else
				{
					if (PyTuple_GET_SIZE(args) > 1)
						return create_list_functor(d, 1, PyTuple_GET_SIZE(args), [args](Py_ssize_t index) -> PyObject * { return PyTuple_GET_ITEM(args, index); });
				}

				throw std::invalid_argument("Empty lists not allowed.");
			}
		}

		throw std::invalid_argument("Dimension expected.");
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::create_range(PyObject * self, PyObject * args)
{
	(void) self;

	try
	{
		number start, end, step;
		if (PyArg_ParseTuple(args, "ddd", &start, &end, &step))
			return create_functor<functors::range>(start, end, step);
		else
			return nullptr;
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyMODINIT_FUNC PyInit_abaq()
{
	PyObject *m = PyModule_Create(&abaq_module);
	if (m)
	{
		// register module types
		python::attribute::register_type(m);
		python::capture::register_type(m);
		python::dimension::register_type(m);
		python::engine::register_type(m);
		python::shape::register_type(m);
		python::tensor::register_type(m);
		//object::register_type(m);

		// register module for further lookup
		PyState_AddModule(m, &abaq_module);

		import_array();
	}

	return m;
}

