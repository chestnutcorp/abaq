/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Aug 27, 2020
 */
#pragma once

#include "abaq/compute/engine.h"

#include <Python.h>


namespace abaq
{
	namespace python
	{
		struct engine : public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject * m);


			compute::engine::ptr engine_;


			static engine * cast(PyObject * self);
			static PyObject * wrap(PyTypeObject * subtype, compute::engine::ptr const &e);

			// support
			static void dealloc(PyObject * self);
			static PyObject * create(PyTypeObject * subtype, PyObject * args, PyObject * kwds);

			// methods
			static PyObject * list(PyObject * self, PyObject * args);
		};
	}
}
