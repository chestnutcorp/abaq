/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */

#include "helpers.h"
#include "tensor.h"

using namespace abaq;


PyObject *python::wrap(graph::functor_base *f)
{
	graph::outputs<void> o(f->get_outputs());
	if (o.size() > 1)
	{
		auto it = o.begin();

		PyObject *r = PyTuple_New(o.size());
		for (size_t i = 0; i < o.size(); ++i)
			PyTuple_SetItem(r, i, tensor::wrap(*it++));
		return r;
	}
	else if (o.size() == 1)
	{
		return tensor::wrap(o.front());
	}
	else
	{
		Py_RETURN_NONE;
	}
}
