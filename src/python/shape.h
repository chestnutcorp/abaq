/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */
#pragma once

#include "abaq/graph/shape.h"

#include <Python.h>

namespace abaq
{
	namespace python
	{
		struct shape : public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject *m);


			graph::shape shape_;


			static shape *cast(PyObject *self);
			static PyObject *wrap(const graph::shape &s);

			// support
			static void dealloc(PyObject *self);
			static PyObject *repr(PyObject *self);
			static PyObject *compare(PyObject *obj1, PyObject *obj2, int op);

			// methods
			static PyObject *rank(PyObject *self, PyObject *args);
			static PyObject *volume(PyObject *self, PyObject *args);

			// number methods
			static PyObject *add(PyObject *obj1, PyObject *obj2);
			static PyObject *sub(PyObject *obj1, PyObject *obj2);
			static PyObject *mul(PyObject *obj1, PyObject *obj2);
		};
	}
}
