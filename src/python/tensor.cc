/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */
#include "abaq/functors/number.h"
#include "abaq/functors/string.h"
#include "abaq/functors/core.h"
#include "abaq/functors/arithmetic.h"
#include "abaq/functors/compare.h"

#include "shape.h"
#include "tensor.h"

#include <exception>
#include <map>
#include <sstream>
#include <string_view>

using namespace abaq;

#define ABAQ_TENSOR "Tensor"

static PyMethodDef methods[] = {
	{
		"shape", python::tensor::get_shape, METH_NOARGS,
		"Returns tensor shape."
	},
	{
		"rank", python::tensor::get_rank, METH_NOARGS,
		"Returns tensor rank (number of dimensions)."
	},
	{
		"volume", python::tensor::get_volume, METH_NOARGS,
		"Returns tensor volume or 0 if volume is not known."
	},
	{
		"tonumber", python::tensor::tonumber, METH_NOARGS,
		"Converts to numeric tensor."
	},
	{
		"toupper", python::tensor::toupper, METH_NOARGS,
		"Converts string tensor to upper case."
	},
	{
		"iif", python::tensor::iif, METH_VARARGS,
		"Conditional expression."
	},
	{
		"regex", python::tensor::regex, METH_VARARGS,
		"Matches against regex pattern."
	},
	{
		"floor", python::tensor::floor, METH_NOARGS,
		"Rounds to max integer less than argument."
	},
	{
		"reduce", python::tensor::reduce, METH_VARARGS,
		"Performs tensor reduction by the specified shape."
	},
	{ nullptr, nullptr, 0, nullptr }
};

static PyNumberMethods number_methods = {
	.nb_add = python::tensor::binvar<functors::add_op, functors::concat>,
	.nb_subtract = python::tensor::binary<functors::sub_op, number>,
	.nb_multiply = python::tensor::binary<functors::mul_op, number>,
	.nb_remainder = python::tensor::binary<functors::rem_op, number>,
	.nb_and = python::tensor::binary<functors::and_op, number>,
	.nb_xor = python::tensor::binary<functors::xor_op, number>,
	.nb_or = python::tensor::binary<functors::or_op, number>,
	.nb_floor_divide = python::tensor::binuni<functors::div_op, functors::floor_op, number>,
	.nb_true_divide = python::tensor::binary<functors::div_op, number>,
};

PyTypeObject python::tensor::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.Tensor",
	.tp_doc = "Multi-dimensional array of values",
	.tp_basicsize = sizeof(python::tensor),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_dealloc = python::tensor::dealloc,
	.tp_as_number = &number_methods,
	.tp_hash = python::tensor::hash,
	.tp_richcompare = python::tensor::compare,
	.tp_methods = methods,
};

bool python::tensor::register_type(PyObject * m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, ABAQ_TENSOR, (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

graph::tensor<void> python::tensor::convert(PyObject * obj, bool strict)
{
	if (PyObject_TypeCheck(obj, &type))
	{
		auto t = static_cast<tensor *>(obj);
		return t->tensor_;
	}

	if (!strict)
	{
		if (PyFloat_Check(obj) || PyLong_Check(obj))
		{
			auto f = graph::factory::get_default_factory()->create<functors::immediate<number>>(PyFloat_AsDouble(obj));
			return f->get_outputs().front();
		}

		if (PyUnicode_Check(obj))
		{
			auto f = graph::factory::get_default_factory()->create<functors::immediate<string>>(PyUnicode_AsUTF8(obj));
			return f->get_outputs().front();
		}
	}

	throw type_exception();
}

PyObject * python::tensor::wrap(graph::tensor<void> const &t)
{
	auto obj = static_cast<tensor *>(type.tp_alloc(&type, 0));
	if (obj)
	{
		new (&obj->tensor_) graph::tensor(t);
	}

	return obj;
}

void python::tensor::dealloc(PyObject * self)
{
	auto obj = static_cast<tensor *>(self);
	obj->tensor_.~tensor();

	Py_TYPE(self)->tp_free(self);
}

PyObject * python::tensor::compare(PyObject * self, PyObject * other, int op)
{
	try
	{
		auto t1 = cast<void>(self);
		auto t2 = cast<void>(other);

		if (t1.type() == value_type::number && t2.type() == value_type::number)
		{
			switch (op)
			{
			case Py_LT: return create_functor<functors::num_lt_op>(t1.cast<number>(), t2.cast<number>());
			case Py_LE: return create_functor<functors::num_le_op>(t1.cast<number>(), t2.cast<number>());
			case Py_EQ: return create_functor<functors::num_eq_op>(t1.cast<number>(), t2.cast<number>());
			case Py_NE: return create_functor<functors::num_ne_op>(t1.cast<number>(), t2.cast<number>());
			case Py_GT: return create_functor<functors::num_gt_op>(t1.cast<number>(), t2.cast<number>());
			case Py_GE: return create_functor<functors::num_ge_op>(t1.cast<number>(), t2.cast<number>());
			}
		}

		if (t1.type() == value_type::string && t2.type() == value_type::string)
		{
			switch (op)
			{
			case Py_LT: return create_functor<functors::str_lt_op>(t1.cast<string>(), t2.cast<string>());
			case Py_LE: return create_functor<functors::str_le_op>(t1.cast<string>(), t2.cast<string>());
			case Py_EQ: return create_functor<functors::str_eq_op>(t1.cast<string>(), t2.cast<string>());
			case Py_NE: return create_functor<functors::str_ne_op>(t1.cast<string>(), t2.cast<string>());
			case Py_GT: return create_functor<functors::str_gt_op>(t1.cast<string>(), t2.cast<string>());
			case Py_GE: return create_functor<functors::str_ge_op>(t1.cast<string>(), t2.cast<string>());
			}
		}

		PyErr_BadArgument();
		return nullptr;
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

Py_hash_t python::tensor::hash(PyObject * self)
{
	try
	{
		return cast<void>(self).hash_code();
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject * python::tensor::get_shape(PyObject * self, PyObject *)
{
	try
	{
		return python::shape::wrap(cast<void>(self).get_shape());
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::get_rank(PyObject * self, PyObject *)
{
	try
	{
		return PyLong_FromSize_t(cast<void>(self).rank());
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::get_volume(PyObject * self, PyObject *)
{
	try
	{
		return PyLong_FromSize_t(cast<void>(self).volume());
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::tonumber(PyObject * self, PyObject *)
{
	try
	{
		return create_functor<functors::tonumber>(cast<string>(self));
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::toupper(PyObject * self, PyObject *)
{
	try
	{
		return create_functor<functors::toupper>(cast<string>(self));
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::iif(PyObject * self, PyObject * args)
{
	try
	{
		PyObject * tobj = nullptr;
		PyObject * fobj = nullptr;

		if (PyArg_ParseTuple(args, "OO", &tobj, &fobj))
		{
			auto t = cast<void>(tobj);
			auto f = cast<void>(fobj);

			if (t.type() == value_type::number && f.type() == value_type::number)
				return create_functor<functors::iif<number>>(cast<number>(self), t.cast<number>(), f.cast<number>());

			if (t.type() == value_type::string && f.type() == value_type::string)
				return create_functor<functors::iif<string>>(cast<number>(self), t.cast<string>(), f.cast<string>());

			PyErr_BadArgument();
		}

		return nullptr;
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::regex(PyObject * self, PyObject * args)
{
	try
	{
		char const * pattern;
		Py_ssize_t nmatches;

		if (PyArg_ParseTuple(args, "sn", &pattern, &nmatches))
		{
			return create_functor<functors::regex>(cast<string>(self), pattern, nmatches);
		}
		else
			return nullptr;
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::floor(PyObject * self, PyObject *)
{
	try
	{
		return create_functor<functors::floor_op>(cast<number>(self));
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

PyObject * python::tensor::reduce(PyObject * self, PyObject * args)
{
	static const std::map<std::string_view, functors::reduce_op::cmd> reduce_op{
		{ "sum", functors::reduce_op::ADD },
		{ "and", functors::reduce_op::AND },
		{ "or",  functors::reduce_op::OR  }
	};

	try
	{
		shape * s;
		char const * o;

		if (PyArg_ParseTuple(args, "O!s", &shape::type, &s, &o))
		{
			auto it = reduce_op.find(o);
			if (it == reduce_op.end())
				throw std::runtime_error("Invalid reduce operation");

			return create_functor<functors::reduce_op>(cast<number>(self), s->shape_, it->second);
		}
	}
	catch (std::exception const &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
	}

	return nullptr;
}
