/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Sep 10, 2020
 */
#pragma once

#include <string>

#include <Python.h>

namespace abaq
{
	namespace python
	{
		struct attribute : public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject * m);

			PyObject * func_;
			std::string name_;

			bool override_;
			bool diddle_;

			// support
			static void dealloc(PyObject * self);
			static PyObject * create(PyTypeObject * subtype, PyObject * args, PyObject * kwds);
			static PyObject * call(PyObject * self, PyObject * args, PyObject * kwds);
			static PyObject * get(PyObject * self, PyObject * instance, PyObject * type);
			static PyObject * lambda(PyObject * data, PyObject *);
		};
	}
}
