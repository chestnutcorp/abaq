/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */
#pragma once

#include <Python.h>

#define ABAQ_MODULE    "abaq"
#define ABAQ_SHAPE     "Shape"
#define ABAQ_TENSOR    "Tensor"
#define ABAQ_DIMENSION "Dimension"

namespace abaq
{
	namespace python
	{
		// module functions
		PyObject * create_number(PyObject * self, PyObject * args);
		PyObject * create_string(PyObject * self, PyObject * args);
		PyObject * create_serial(PyObject * self, PyObject * args);
		PyObject * create_pack(PyObject * self, PyObject * args);
		PyObject * create_list(PyObject * self, PyObject * args);
		PyObject * create_range(PyObject * self, PyObject * args);
	}
}
