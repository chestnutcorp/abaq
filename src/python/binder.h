/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \date May 26, 2024
 */
#pragma once

#include "abaq/compute/binder.h"

//#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarraytypes.h>

namespace abaq::python
{
	class binder : public compute::binder
	{
		PyArrayObject * obj_;

	public:
		binder(PyObject * obj);

		virtual ~binder();

		virtual void bind(compute::membuf::ptr const &buf, size_t const * ids, compute::context * ctx) override;
	};
}

