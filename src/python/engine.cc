/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com
 * \since Aug 27, 2020
 */

#include "engine.h"
#include "tensor.h"

#include <exception>

using namespace abaq;


static PyMethodDef methods[] = {
	{
		"list", python::engine::list, METH_NOARGS | METH_STATIC,
		"Returns a { name, description } dictionary of available engines."
	},
	{ nullptr, nullptr, 0, nullptr }
};

PyTypeObject python::engine::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.Engine",
	.tp_doc = "Engine that performs computations.",
	.tp_basicsize = sizeof(python::engine),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_dealloc = python::engine::dealloc,
	.tp_methods = methods,
	.tp_new = python::engine::create,
};

bool python::engine::register_type(PyObject * m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, "Engine", (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

python::engine * python::engine::cast(PyObject * self)
{
	return Py_TYPE(self) == &type ? static_cast<engine *>(self) : nullptr;
}

PyObject * python::engine::wrap(PyTypeObject * subtype, compute::engine::ptr const &e)
{
	auto obj = static_cast<engine *>(subtype->tp_alloc(subtype, 0));
	if (obj)
	{
		new (&obj->engine_) compute::engine::ptr(e);
	}

	return obj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void python::engine::dealloc(PyObject * self)
{
	auto obj = static_cast<engine *>(self);
	obj->engine_.~shared_ptr();

	Py_TYPE(self)->tp_free(self);
}

PyObject * python::engine::create(PyTypeObject * subtype, PyObject * args, PyObject * kwds)
{
	(void) kwds;

	PyObject * result = nullptr;

	char const * spec = nullptr;

	if (PyArg_ParseTuple(args, "|s", &spec))
	{
		try
		{
			result = wrap(subtype, compute::engine::create(spec ? spec : "local"));
		}
		catch (std::exception const &ex)
		{
			PyErr_SetString(PyExc_RuntimeError, ex.what());
		}
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject * python::engine::list(PyObject * self, PyObject * args)
{
	(void) self;
	(void) args;

	PyObject * result = PyDict_New();

	for (auto const &p : compute::engine::list())
	{
		PyObject * desc = PyUnicode_FromStringAndSize(p.second.data(), p.second.size());
		PyDict_SetItemString(result, p.first.data(), desc);
		Py_DECREF(desc);
	}

	return result;
}

/*

	try
	{
		auto obj = cast(self);
		if (obj)
		{
			graph::inputs<void> vars;

			for (Py_ssize_t index = 0, count = PyTuple_Size(args); index < count; ++index)
			{
				PyObject *item = PyTuple_GetItem(args, index);
				vars.push_back(tensor::convert(item, true));
			}

			auto s = obj->engine_->capture(vars);
			return session::wrap(s);
		}
		else
			return nullptr;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
*/
