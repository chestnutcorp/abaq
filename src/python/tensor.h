/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */
#pragma once

#include <Python.h>

#include "abaq/graph/tensor.h"

#include "helpers.h"


namespace abaq
{
	namespace python
	{
		struct tensor : public PyObject
		{
			static PyTypeObject type;
			static bool register_type(PyObject * m);


			graph::tensor<void> tensor_;


			template <typename T> static graph::tensor<T> cast(PyObject * o) { return convert(o).cast<T>(); }

			/*!
			 * \brief Unwraps tensor object provided by Python wrapper.
			 *
			 * In non-strict mode (default) it also converts scalars to appropriate tensors.
			 *
			 * \param o wrapper Python object
			 * \param strict whether conversion is strict or not
			 * \return tensor object
			 * \throws type exception if Python object doesn't represent a tensor.
			 */
			static graph::tensor<void> convert(PyObject * o, bool strict = false);

			/*!
			 * \brief Wraps a tensor into Python object.
			 *
			 * \param t tensor
			 * \return Python wrapper object
			 */
			static PyObject * wrap(const graph::tensor<void> &t);

			// support
			static void dealloc(PyObject * self);
			static PyObject * compare(PyObject * self, PyObject * other, int op);
			static Py_hash_t hash(PyObject * self);

			// methods
			static PyObject * get_shape(PyObject * self, PyObject * args);
			static PyObject * get_rank(PyObject * self, PyObject * args);
			static PyObject * get_volume(PyObject * self, PyObject * args);
			static PyObject * tonumber(PyObject * self, PyObject * args);
			static PyObject * toupper(PyObject * self, PyObject * args);
			static PyObject * iif(PyObject * self, PyObject * args);
			static PyObject * regex(PyObject * self, PyObject * args);
			static PyObject * floor(PyObject * self, PyObject * args);
			static PyObject * reduce(PyObject * self, PyObject * args);


			template <typename F, typename T>
			static PyObject * unary(PyObject * obj)
			{
				try
				{
					return create_functor<F>(cast<T>(obj));
				}
				catch (std::exception const &ex)
				{
					PyErr_SetString(PyExc_RuntimeError, ex.what());
					return nullptr;
				}
			}

			template <typename F, typename T>
			static PyObject * binary(PyObject * obj1, PyObject * obj2)
			{
				try
				{
					return create_functor<F>(cast<T>(obj1), cast<T>(obj2));
				}
				catch (std::exception const &ex)
				{
					PyErr_SetString(PyExc_RuntimeError, ex.what());
					return nullptr;
				}
			}

			template <typename N, typename S>
			static PyObject * binvar(PyObject * obj1, PyObject * obj2)
			{
				try
				{
					auto t1 = convert(obj1);
					auto t2 = convert(obj2);

					if (t1.type() == value_type::number && t2.type() == value_type::number)
						return create_functor<N>(cast<number>(obj1), cast<number>(obj2));

					if (t1.type() == value_type::string && t2.type() == value_type::string)
						return create_functor<S>(cast<string>(obj1), cast<string>(obj2));

					PyErr_BadArgument();
					return nullptr;
				}
				catch (std::exception const &ex)
				{
					PyErr_SetString(PyExc_RuntimeError, ex.what());
					return nullptr;
				}
			}

			template <typename B, typename U,typename T>
			static PyObject * binuni(PyObject * obj1, PyObject * obj2)
			{
				try
				{
					auto f = graph::factory::get_default_factory()->create<B>(cast<T>(obj1), cast<T>(obj2));
					return create_functor<U>(f->get_outputs().front());
				}
				catch (std::exception const &ex)
				{
					PyErr_SetString(PyExc_RuntimeError, ex.what());
					return nullptr;
				}
			}
		};
	}
}
