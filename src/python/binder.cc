/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \date May 26, 2024
 */
#include "binder.h"

#include "abaq/compute/context.h"
#include "abaq/compute/dimension.h"
#include "abaq/compute/indexer.h"

#include <stdexcept>

#include <numpy/ndarraytypes.h>
#include <numpy/ndarrayobject.h>

using namespace abaq;

python::binder::binder(PyObject * obj)
{
	//if (!PyArray_Check(obj))
	//	throw std::runtime_error("NumPy array expected.");

	obj_ = (PyArrayObject *) obj;

	if (PyArray_TYPE(obj_) != NPY_DOUBLE)
		throw std::runtime_error("Unexpected NumPy array type.");

	Py_INCREF(obj_);
}

python::binder::~binder()
{
	Py_DECREF(obj_);
}

void python::binder::bind(compute::membuf::ptr const &buf, size_t const * ids, compute::context * ctx)
{
	if (static_cast<size_t>(PyArray_NDIM(obj_)) != buf->dimensions().size())
		throw std::runtime_error("Shape mismatch.");

	compute::dimension::list dims;
	{
		auto npy_dims = PyArray_DIMS(obj_);
		for (size_t index = 0, count = buf->dimensions().size(); index < count; ++index)
			dims.emplace_back(ids[index], npy_dims[index]);
	}

	ctx->override_dims(dims); // make sure dimensions match
	{
		auto data = buf->num_data();

		compute::indexer isrc(dims);
		compute::indexer idst(buf->dimensions());

		for (size_t index = 0, count = buf->size(); index < count; ++index)
		{
			idst.set_index(index);
			isrc.fit(idst);

			//TODO: revise me
			auto inp = isrc.coords();

			size_t ofs = 0;
			for (int i = 0; i < obj_->nd; ++i)
				ofs += obj_->strides[i] * inp[i];
			auto ptr = obj_->data + ofs;
			data[index] = *reinterpret_cast<double *>(ptr);
		}
	}
}

