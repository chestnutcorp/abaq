/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com
 * \since Mar 8, 2022
 */
#include "feed.h"

#include <exception>
#include <iostream>
#include <sstream>

using namespace abaq;

PyTypeObject python::functors::feed::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.functors.Feed",
	.tp_doc = "Feed functor.",
	.tp_basicsize = sizeof(python::functors::feed),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_dealloc = python::functors::feed::dealloc,
	.tp_new = python::functors::feed::create,
	.tp_call = python::functors::feed::call,
};

bool python::functors::feed::register_type(PyObject * m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, "Feed", (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void python::functors::feed::dealloc(PyObject * self)
{
	std::cout << "dealloc attr" << std::endl;

	auto obj = static_cast<functors::feed *>(self);

	Py_XDECREF(obj->func_);

	obj->name_.std::string::~string();

	Py_TYPE(self)->tp_free(self);
}

PyObject * python::functors::feed::create(PyTypeObject * subtype, PyObject * args, PyObject * kwds)
{
	static char const * const keywords[] = { "override", "diddle", nullptr };

	PyObject * result = nullptr;
	int overridable = 0, diddlable = 0;

	if (PyArg_ParseTupleAndKeywords(args, kwds, "pp", const_cast<char **>(keywords), &overridable, &diddlable))
	{
		result = subtype->tp_alloc(subtype, 0);
		if (result)
		{
			auto obj = static_cast<functors::feed *>(result);
			obj->func_ = nullptr;
			obj->override_ = overridable;
			obj->diddle_ = diddlable;

			new (&obj->name_) std::string();
		}
	}

	return result;
}

PyObject * python::functors::feed::call(PyObject * self, PyObject * args, PyObject * kwds)
{
	(void) kwds;

	auto obj = static_cast<attribute *>(self);
	if (obj->func_)
	{
		PyErr_SetString(PyExc_RuntimeError, "abaq.Attribute must not be called.");
		return nullptr;
	}
	else
	{
		obj->func_ = PyTuple_GetItem(args, 0);
		Py_INCREF(obj->func_);

		auto name = PyObject_GetAttrString(obj->func_, "__name__");
		if (name)
			obj->name_ = PyUnicode_AsUTF8(name);

		Py_INCREF(self);
		return self;
	}
}

PyObject * python::attribute::get(PyObject * self, PyObject * instance, PyObject * type)
{
	static PyMethodDef lambda = { "lambda", python::attribute::lambda, METH_NOARGS, "Attribute value wrapper." };

	(void) type;

	auto obj = static_cast<attribute *>(self);
	if (obj->func_)
	{
		PyObject * data = PyTuple_Pack(2, self, instance);
		PyObject * func = PyCFunction_New(&lambda, data);

		return func;
	}
	else
	{
		PyErr_SetString(PyExc_RuntimeError, "Unexpected abaq.Attribute decorator use.");
		return nullptr;
	}
}

PyObject * python::attribute::lambda(PyObject * data, PyObject *)
{
	PyObject * self = PyTuple_GET_ITEM(data, 0);
	PyObject * instance = PyTuple_GET_ITEM(data, 1);
	
	auto obj = static_cast<attribute *>(self);

	PyObject * result =  PyObject_CallOneArg(obj->func_, instance);

	Py_DECREF(data);
	return result;
	//return PyLong_FromLong(2);
}
