/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Nov 1, 2022
 */
#pragma once

#include <string>

#include <Python.h>

namespace abaq
{
	namespace python
	{
		namespace functors
		{
			struct feed : public PyObject
			{
				static PyTypeObject type;
				static bool register_type(PyObject * m);

				// support
				static void dealloc(PyObject * self);
				static PyObject * create(PyTypeObject * subtype, PyObject * args, PyObject * kwds);
				static PyObject * call(PyObject * self, PyObject * args, PyObject * kwds);
			};
		}
	}
}

