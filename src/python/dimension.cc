/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com
 * \since May 11, 2021
 */

#include "dimension.h"
#include "abaq/abaq.h"

#include <exception>
#include <sstream>

using namespace abaq;


static PyMethodDef methods[] = {
	{
		"size", python::dimension::size, METH_NOARGS,
		"Returns dimension size."
	},
	{ nullptr, nullptr, 0, nullptr }
};

PyTypeObject python::dimension::type = {
	PyVarObject_HEAD_INIT(nullptr, 0)
	.tp_name = "abaq.Dimension",
	.tp_doc = "Tensor dimension.",
	.tp_basicsize = sizeof(python::dimension),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_new = python::dimension::create,
	.tp_dealloc = python::dimension::dealloc,
	.tp_repr = python::dimension::repr,
	.tp_richcompare = python::dimension::compare,
	.tp_methods = methods,
};

bool python::dimension::register_type(PyObject *m)
{
	if (PyType_Ready(&type) == 0)
	{
		Py_INCREF(&type);
		PyModule_AddObject(m, "Dimension", (PyObject *) &type);
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

python::dimension *python::dimension::cast(PyObject *self)
{
	return Py_TYPE(self) == &type ? static_cast<dimension *>(self) : nullptr;
}

PyObject *python::dimension::wrap(const graph::dimension &d)
{
	auto obj = static_cast<dimension *>(type.tp_alloc(&type, 0));
	if (obj)
	{
		new (&obj->dimension_) graph::dimension(d);
	}

	return obj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject *python::dimension::create(PyTypeObject *subtype, PyObject *args, PyObject *kwds)
{
	(void) kwds;

	auto obj = static_cast<dimension *>(subtype->tp_alloc(subtype, 0));
	if (obj)
	{
		auto count = PyTuple_GET_SIZE(args);
		if (count == 0)
		{
			new (&obj->dimension_) graph::dimension();
		}
		else if (count == 1 && PyLong_Check(PyTuple_GET_ITEM(args, 0)))
		{
			new (&obj->dimension_) graph::dimension(PyLong_AsSize_t(PyTuple_GET_ITEM(args, 0)));
		}
		else
		{
			PyErr_SetString(PyExc_TypeError, "Expected dimension size.");

			subtype->tp_free(obj);
			return nullptr;
		}
	}

	return obj;
}

void python::dimension::dealloc(PyObject *self)
{
	auto obj = static_cast<dimension *>(self);
	obj->dimension_.~dimension();

	Py_TYPE(self)->tp_free(self);
}

PyObject *python::dimension::repr(PyObject *self)
{
	auto obj = cast(self);
	if (obj)
	{
		std::ostringstream buf;
		buf << "<dimension " << obj->dimension_ << '>';;
		
		auto repr = buf.str();
		return PyUnicode_FromStringAndSize(repr.data(), repr.size());
	}
	else
		return nullptr;
}

PyObject *python::dimension::compare(PyObject *obj1, PyObject *obj2, int op)
{
	try
	{
		bool res = false;

		dimension *s1 = cast(obj1);
		dimension *s2 = cast(obj2);

		if (s1 && s2)
		{
			switch (op)
			{
				case Py_EQ: res = s1->dimension_ == s2->dimension_; break;
				case Py_NE: res = s1->dimension_ != s2->dimension_; break;
			}
		}

		PyObject *result = res ? Py_True : Py_False;
		Py_INCREF(result);
		return result;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PyObject *python::dimension::size(PyObject *self, PyObject *args)
{
	(void) args;

	try
	{
		dimension *s = cast(self);
		if (s)
			return PyLong_FromSize_t(s->dimension_.size());
		else
			return nullptr;
	}
	catch (const std::exception &ex)
	{
		PyErr_SetString(PyExc_RuntimeError, ex.what());
		return nullptr;
	}
}
