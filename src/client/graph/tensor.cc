/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 9, 2020
 */

#include "abaq/graph/tensor.h"
#include "abaq/graph/functor.h"
#include "abaq/graph/factory.h"

using namespace abaq;


graph::tensor_base::tensor_base(functor_base * f, int id) : functor_(f), id_(id)
{
	functor_->add_ref();
}

graph::tensor_base::tensor_base(const tensor_base &other) : functor_(other.functor_), id_(other.id_)
{
	functor_->add_ref();
}

graph::tensor_base::~tensor_base()
{
	dispose();
}

void graph::tensor_base::assign(const tensor_base &other)
{
	dispose();

	functor_ = other.functor_;
	functor_->add_ref();

	id_ = other.id_;
}

void graph::tensor_base::dispose()
{
	if (functor_->release() == 0)
	{
		factory *f = functor_->get_factory();
		f->dispose(functor_);
	}
}

void graph::tensor_base::check_type(value_type t)
{
	if (functor_->type() != t)
		throw type_exception();
}

size_t graph::tensor_base::hash_code() const
{
	return 3 * std::hash<functor_base *>{}(functor_) + std::hash<int>{}(id_);
}

value_type graph::tensor_base::type() const
{
	return functor_->type();
}

const graph::shape &graph::tensor_base::get_shape() const
{
	return functor_->get_shape();
}

size_t graph::tensor_base::rank() const
{
	return functor_->get_shape().rank();
}

size_t graph::tensor_base::volume() const
{
	return functor_->get_shape().volume();
}
