/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 9, 2021
 */

#include "abaq/graph/capture.h"

using namespace abaq;

graph::capture::capture(compute::engine::ptr const &e, inputs<void> const &vars) : ctx_(e->create_context())
{
	unit_.build(vars);

	for (auto const &t : vars)
		vars_allocs_.emplace(t, unit_.get_index(t));

	unit_.deploy(ctx_);
}

void graph::capture::eval(compute::binder::map const &binds)
{
	ctx_->eval(binds);
}

compute::membuf::ptr graph::capture::get_membuf(tensor<void> const &t)
{
	auto it = vars_allocs_.find(t);
	if (it != vars_allocs_.end())
		return ctx_->get_membuf(it->second);
	else
		return nullptr;
}
