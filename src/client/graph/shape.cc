/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 8, 2020
 */

#include "abaq/graph/shape.h"

#include <algorithm>
#include <numeric>

using namespace abaq;

graph::shape::shape(dimension const &d)
{
	dims_.emplace(d);
}

graph::shape::shape(shape const &s1, shape const &s2)
{
	append(s1);
	append(s2);
}

size_t graph::shape::hash_code() const
{
	return std::reduce(dims_.begin(), dims_.end(), 7, [](auto hash, auto const &d) { return 5 * hash + d.hash_code(); });
}

void graph::shape::append(const shape &s)
{
	dims_.insert(s.dims_.begin(), s.dims_.end());
}

size_t graph::shape::volume() const
{
	if (dims_.empty())
		return 0;
	else
		return std::reduce(dims_.begin(), dims_.end(), 1, [](auto vol, auto const &d) { return vol * d.size(); });
}

graph::dimension::list graph::shape::dims() const
{
	dimension::list dims(dims_.begin(), dims_.end());
	std::sort(dims.begin(), dims.end(), dimension::order{});
	return dims;
}

graph::shape graph::operator + (const shape &lhs, const shape &rhs)
{
	dimension::list dims;
	std::set_union(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::back_inserter(dims));

	return shape(dims);
}

graph::shape graph::operator - (const shape &lhs, const shape &rhs)
{
	dimension::list dims;
	std::set_difference(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::back_inserter(dims));

	return shape(dims);
}

graph::shape graph::operator * (const shape &lhs, const shape &rhs)
{
	dimension::list dims;
	std::set_intersection(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::back_inserter(dims));

	return shape(dims);
}

