/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 9, 2020
 */
#include "abaq/graph/dimension.h"
#include "abaq/abaq.h"

#include <algorithm>
#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <shared_mutex>
#include <utility>

using namespace abaq;

std::atomic_size_t graph::dimension::gen(0);

std::list<std::weak_ptr<graph::dimension::data>> graph::dimension::dimensions;
std::shared_mutex graph::dimension::dimensions_mutex;

std::unordered_multimap<size_t, size_t> graph::dimension::orders;
std::shared_mutex graph::dimension::orders_mutex;


bool graph::dimension::order::operator () (dimension const &lhs, dimension const &rhs) const
{
	std::shared_lock lock(orders_mutex);

	{
		auto r = orders.equal_range(lhs.data_->id);
		if (std::any_of(r.first, r.second, [id = rhs.data_->id](auto const &o) { return o.second == id; }))
			return true;
	}

	{
		auto r = orders.equal_range(rhs.data_->id);
		if (std::any_of(r.first, r.second, [id = lhs.data_->id](auto const &o) { return o.second == id; }))
			return false;
	}

	return lhs.data_->id > rhs.data_->id;
}


graph::dimension::dimension(size_t size) : data_(std::make_shared<data>(++gen, size))
{
	std::unique_lock lock(dimensions_mutex);
	dimensions.emplace_back(data_);
}

void graph::dimension::ensure_size(size_t size) const
{
	if (data_->size == 0 || data_->size == size)
		data_->size = size;
	else
		throw dimension_exception("Dimension size mismatch.");
}

void graph::dimension::subordinate(set const &dims)
{
	std::set<size_t> live_ids;
	{
		// obtain a list of all live dimension ids
		std::unique_lock lock(dimensions_mutex);
		for (auto it = dimensions.begin(); it != dimensions.end(); )
		{
			if (auto data = it->lock())
			{
				live_ids.insert(data->id);
				++it;
			}
			else
				it = dimensions.erase(it);
		}
	}


	std::unique_lock lock(orders_mutex);

	// cleanup order from dead dimensions
	for (auto it = orders.begin(); it != orders.end(); )
	{
		if (live_ids.contains(it->first) && live_ids.contains(it->second))
			++it;
		else
			it = orders.erase(it);
	}

	// apply subordinates
	for (auto const &d : dims)
	{
		if (set_order_locked(data_->id, d.data_->id)) // if ordering is applied...
		{
			// ... apply it to all members of transitive closure, making new transitive closure
			auto r = orders.equal_range(d.data_->id);
			std::for_each(r.first, r.second, [this](auto const &o) { set_order_locked(data_->id, o.second); });
		}
	}
}

size_t graph::dimension::hash_code() const
{
	return std::hash<size_t>{}(data_->id);
}

void graph::dimension::output(std::ostream &os) const
{
	os << 'D' << id();
}

bool graph::dimension::set_order_locked(size_t a, size_t b)
{
	auto rb = orders.equal_range(b);
	if (std::any_of(rb.first, rb.second, [a](auto const &o) { return o.second == a; }))
		throw dimension_exception("Dimension order conflict.");

	auto ra = orders.equal_range(a);
	if (std::any_of(ra.first, ra.second, [b](auto const &o) { return o.second == b; }))
		return false;

	orders.emplace(a, b);
	return true;
}

