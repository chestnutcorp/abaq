/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 9, 2020
 */

#include "abaq/graph/functor.h"
#include "abaq/graph/tensor.h"

#include <typeinfo>

using namespace abaq;


graph::functor_base::functor_base(factory * f, shape const &s) : ref_count_(0), factory_(f), shape_(s)
{
}

graph::functor_base::~functor_base()
{
	//std::clog << "Destroyed functor " << this << std::endl;
}

size_t graph::functor_base::hash_code() const
{
	size_t hash = 3 * typeid(*this).hash_code() + shape_.hash_code();

	for (auto const &t : get_inputs())
		hash = 3 * hash + t.hash_code();
	return hash;
}

bool graph::functor_base::equal_to(functor_base const * f) const
{
	return typeid(*this) == typeid(*f) && shape_ == f->shape_ && get_inputs() == f->get_inputs();
}

bool graph::functor_base::is_single_output()
{
	return get_outputs().size() == 1; //TODO: optimize me
}
