/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since May 6, 2021
 */

#ifndef ABAQ_GRAPH_DIMENSION2
#define ABAQ_GRAPH_DIMENSION2

#include "abaq/abaq.h"
#include "abaq/graph/dimension.h"

#endif

