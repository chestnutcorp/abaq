/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 9, 2020
 */

#include <abaq/graph/factory.h>

using namespace abaq;


graph::factory *graph::factory::get_default_factory()
{
	static factory default_factory;
	return &default_factory;
}

void graph::factory::dispose(functor_base *f)
{
	functor_ptr p(f);

	functors_.erase(p);
	(void) p.release();
}
