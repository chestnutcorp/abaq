/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 30, 2021
 */
#pragma once

#include "chain.h"

#include <map>

namespace abaq::compile
{
	/*!
	 * Processes captured tensors and builds a plan how to evaluate the graph, splitting it on a sequence of
	 * linear chains of evaluation.
	 */
	class scheduler
	{
		chain::list chains_; ///< a list of chains that compose a computation graph
		//std::map<graph::functor_base *, chain::iterator> processed_; ///< maps every processed functor to a chain it belongs to

	public:
		/*! \brief Processes captured tensor and builds chains internally. */
		void process(graph::tensor<void> const &t);

		/*! \brief Returns chains begin iterator */
		auto begin() { return chains_.begin(); }

		/*! \brief Returns chains end iterator */
		auto end() { return chains_.end(); }

	private:
		chain weave(graph::tensor<void> const &t);

		void split_chain(chain::iterator ci, graph::tensor<void> const &t);

		chain::iterator find_chain(graph::tensor<void> const &t);
	};
}
