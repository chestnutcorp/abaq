/*!
 * \author Yuri Plaksyuk <yuri.plaksyuck@chestnutcorp.com>
 * \since Jun 30, 2021
 */
#pragma once

#include "abaq/graph/tensor.h"

#include <list>

namespace abaq::compile
{
	/*! \brief A linear sequence of tensors that evaluate one from the other.
	 *
	 * The `chain` represents a sequence of tensors that evaluate one from the other
	 * and have the same shape, so that operations can be performed on the same single `membuf`.
	 */
	class chain
	{
		/*! \brief Tensors list in reverse order.
		 *
		 * Most recently evaluated tensor appears at first position.
		 */
		std::list<graph::tensor<void>> links_;

	public:
		using list = std::list<chain>;
		using iterator = list::iterator;

		/*! \brief Create a chain from a single tensor.
		 *
		 * \param t tensor
		 */
		chain(graph::tensor<void> const &t);

		/*! \brief Create new chain splitting the other one by tensor `t`.
		 *
		 * \param t tensor by which to split apart
		 * \param other the chain to split by
		 */
		chain(graph::tensor<void> const &t, chain &other);

		/*! \brief Create new chain taking out its body from the chains.
		 *
		 * Creates a new chain composed of a body and the specified tensor. The body is taken
		 * from the chains list specified by the iterator `ci`.
		 *
		 * \param t tensor as a tail
		 * \param ci iterator pointer of the body
		 * \params chains a list of chains
		 */
		chain(graph::tensor<void> const &t, chain::iterator ci, chain::list &chains);

		/*! \brief Default copy constructor. */
		chain(chain const &other) = default;

		/*! \brief Default move constructor */
		chain(chain &&other) = default;

		/*! \brief Returns chain length. */
		size_t size() const { return links_.size(); }

		/*! \brief Returns last (most recently evaluated) functor of the chain. */
		graph::functor_base * get_tail_functor() const { return links_.front().get_functor(); }

		/*! \brief Check whether a specified functor belongs to this chain. */
		bool contains(graph::functor_base * f) const;

		/*! \brief Get links in reverse order - most recently evaluated first. */
		std::list<graph::tensor<void>> const &get_links() const { return links_; }

		/*! \brief Checks whether the chain can be linked with the specified tensor. */
		bool can_link(graph::tensor<void> const &t);
	};
}
