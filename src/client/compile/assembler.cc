/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 1, 2021
 */
#include "abaq/compile/assembler.h"
#include "abaq/compile/unit.h"
#include "abaq/compute/opcode.h"

#include <algorithm>
#include <iterator>

using namespace abaq;

size_t compile::assembler::alloc(number value)
{
	auto it = std::find(unit_->nseg_.begin(), unit_->nseg_.end(), value);
	if (it == unit_->nseg_.end())
		it = unit_->nseg_.insert(unit_->nseg_.end(), value);
	return std::distance(unit_->nseg_.begin(), it);
}

size_t compile::assembler::alloc(string const &value)
{
	auto it = std::find(unit_->sseg_.begin(), unit_->sseg_.end(), value);
	if (it == unit_->sseg_.end())
		it = unit_->sseg_.insert(unit_->sseg_.end(), value);
	return std::distance(unit_->sseg_.begin(), it);
}

size_t compile::assembler::alloc(std::vector<number> const &values)
{
	auto it = unit_->nseg_.insert(unit_->nseg_.end(), values.begin(), values.end());
	return std::distance(unit_->nseg_.begin(), it);
}

size_t compile::assembler::alloc(std::vector<string> const &values)
{
	auto it = unit_->sseg_.insert(unit_->sseg_.end(), values.begin(), values.end());
	return std::distance(unit_->sseg_.begin(), it);
}

size_t compile::assembler::get_index(graph::tensor<void> const &t)
{
	return unit_->get_index(t);
}

void compile::assembler::emit(size_t i0)
{
	unit_->cseg_.push_back(i0);
}

void compile::assembler::emit(opcode op, size_t i1)
{
	unit_->cseg_.push_back(static_cast<size_t>(op));
	unit_->cseg_.push_back(i1);
}

void compile::assembler::emit(opcode op, size_t i1, size_t i2)
{
	unit_->cseg_.push_back(static_cast<size_t>(op));
	unit_->cseg_.push_back(i1);
	unit_->cseg_.push_back(i2);
}

void compile::assembler::emit(opcode op, size_t i1, size_t i2, size_t i3)
{
	unit_->cseg_.push_back(static_cast<size_t>(op));
	unit_->cseg_.push_back(i1);
	unit_->cseg_.push_back(i2);
	unit_->cseg_.push_back(i3);
}

void compile::assembler::emit(opcode op, size_t i1, size_t i2, size_t i3, size_t i4)
{
	unit_->cseg_.push_back(static_cast<size_t>(op));
	unit_->cseg_.push_back(i1);
	unit_->cseg_.push_back(i2);
	unit_->cseg_.push_back(i3);
	unit_->cseg_.push_back(i4);
}

void compile::assembler::emit(opcode op, size_t i1, size_t i2, size_t i3, size_t i4, size_t i5, size_t i6, size_t i7, size_t i8)
{
	unit_->cseg_.push_back(static_cast<size_t>(op));
	unit_->cseg_.push_back(i1);
	unit_->cseg_.push_back(i2);
	unit_->cseg_.push_back(i3);
	unit_->cseg_.push_back(i4);
	unit_->cseg_.push_back(i5);
	unit_->cseg_.push_back(i6);
	unit_->cseg_.push_back(i7);
	unit_->cseg_.push_back(i8);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void compile::assembler::num_reshape(graph::tensor<number> const &x, graph::tensor<number> const &a)
{
	emit(opcode::num_reshape, get_index(x), get_index(a));
}

void compile::assembler::num_immediate(graph::tensor<number> const &x, number value)
{
	emit(opcode::num_immediate, get_index(x), alloc(value));
}

void compile::assembler::num_range(graph::tensor<number> const &x, number start, number step)
{
	emit(opcode::num_range, get_index(x), alloc(start), alloc(step));
}

void compile::assembler::num_bind(graph::tensor<number> const &x, graph::dimension::list const &dims)
{
	unit_->anchor();

	emit(opcode::num_bind, get_index(x), dims.size());
	std::for_each(dims.begin(), dims.end(), [this](auto const &d) { emit(d.id()); });
}

void compile::assembler::num_list(graph::tensor<number> const &x, std::vector<number> const &values)
{
	std::cerr << "num_list:";
	std::for_each(values.begin(), values.end(), [](const auto &v) { std::cerr << " " << v; });
	std::cerr << std::endl;
	std::cerr.flush();

	emit(opcode::num_list, get_index(x), alloc(values));
}

void compile::assembler::num_pack(graph::tensor<number> const &x, std::list<graph::tensor<number>> const &inputs)
{
	emit(opcode::num_pack, get_index(x), inputs.size());
	std::for_each(inputs.begin(), inputs.end(), [this](auto const &t) { emit(get_index(t)); });
}

void compile::assembler::num_iif(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b, graph::tensor<number> const &c)
{
	emit(opcode::num_iif, get_index(x), get_index(a), get_index(b), get_index(c));
}

/* arithmetic */
void compile::assembler::num_add(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_add, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_sub(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_sub, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_mul(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_mul, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_div(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_div, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_rem(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_rem, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_and(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_and, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_or(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_or, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_xor(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_xor, get_index(x), get_index(a), get_index(b));
}

/* imm arith */
void compile::assembler::num_imm_add(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_add, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_sub(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_sub, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_mul(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_mul, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_div(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_div, get_index(x), get_index(a), alloc(value));
}

/* compare */
void compile::assembler::num_lt(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_lt, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_le(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_le, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_eq(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_eq, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_ne(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_ne, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_gt(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_gt, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::num_ge(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b)
{
	emit(opcode::num_ge, get_index(x), get_index(a), get_index(b));
}

/* imm comp */
void compile::assembler::num_imm_lt(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_lt, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_le(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_le, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_eq(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_eq, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_ne(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_ne, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_gt(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_gt, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::num_imm_ge(graph::tensor<number> const &x, graph::tensor<number> const &a, number value)
{
	emit(opcode::num_imm_ge, get_index(x), get_index(a), alloc(value));
}

/* math */
void compile::assembler::num_floor(graph::tensor<number> const &x, graph::tensor<number> const &a)
{
	emit(opcode::num_floor, get_index(x), get_index(a));
}

void compile::assembler::num_reduce_add(graph::tensor<number> const &x, graph::tensor<number> const &a)
{
	emit(opcode::num_reduce_add, get_index(x), get_index(a));
}

void compile::assembler::num_reduce_and(graph::tensor<number> const &x, graph::tensor<number> const &a)
{
	emit(opcode::num_reduce_and, get_index(x), get_index(a));
}

void compile::assembler::num_reduce_or(graph::tensor<number> const &x, graph::tensor<number> const &a)
{
	emit(opcode::num_reduce_or, get_index(x), get_index(a));
}

/* serial */
void compile::assembler::num_serial_parse(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &format)
{
	emit(opcode::num_serial_parse, get_index(x), get_index(a), alloc(format));
}

void compile::assembler::num_serial_format(graph::tensor<string> const &x, graph::tensor<number> const &a, string const &format)
{
	emit(opcode::num_serial_format, get_index(x), get_index(a), alloc(format));
}

void compile::assembler::num_serial_pack(graph::tensor<number> const &x, serial_parts const &in)
{
	emit(opcode::num_serial_pack, get_index(x), get_index(in[0]), get_index(in[1]), get_index(in[2]), get_index(in[3]), get_index(in[4]), get_index(in[5]), get_index(in[6]));
}

void compile::assembler::num_serial_unpack(graph::tensor<number> const &a, serial_parts const &out)
{
	emit(opcode::num_serial_pack, get_index(a), get_index(out[0]), get_index(out[1]), get_index(out[2]), get_index(out[3]), get_index(out[4]), get_index(out[5]), get_index(out[6]));
}


/* core */
void compile::assembler::str_reshape(graph::tensor<string> const &x, graph::tensor<string> const &a)
{
	emit(opcode::str_reshape, get_index(x), get_index(a));
}

void compile::assembler::str_literal(graph::tensor<string> const &x, string const &value)
{
	emit(opcode::str_literal, get_index(x), alloc(value));
}

void compile::assembler::str_bind(graph::tensor<string> const &x, graph::dimension::list const &dims)
{
	unit_->anchor();

	emit(opcode::str_bind, get_index(x), dims.size());
	std::for_each(dims.begin(), dims.end(), [this](auto const &d) { emit(d.id()); });
}

void compile::assembler::str_list(graph::tensor<string> const &x, std::vector<string> const &values)
{
	emit(opcode::str_list, get_index(x), alloc(values));
}

void compile::assembler::str_pack(graph::tensor<string> const &x, std::list<graph::tensor<string>> const &inputs)
{
	emit(opcode::str_pack, get_index(x), inputs.size());
	std::for_each(inputs.begin(), inputs.end(), [this](auto const &t) { emit(get_index(t)); });
}

void compile::assembler::str_iif(graph::tensor<string> const &x, graph::tensor<number> const &a, graph::tensor<string> const &b, graph::tensor<string> const &c)
{
	emit(opcode::str_iif, get_index(x), get_index(a), get_index(b), get_index(c));
}

/* compare */
void compile::assembler::str_lt(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_lt, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_le(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_le, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_eq(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_eq, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_ne(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_ne, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_gt(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_gt, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_ge(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_ge, get_index(x), get_index(a), get_index(b));
}

/* imm comp */
void compile::assembler::str_imm_lt(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_lt, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::str_imm_le(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_le, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::str_imm_eq(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_eq, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::str_imm_ne(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_ne, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::str_imm_gt(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_gt, get_index(x), get_index(a), alloc(value));
}

void compile::assembler::str_imm_ge(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value)
{
	emit(opcode::str_imm_ge, get_index(x), get_index(a), alloc(value));
}

/* string */
void compile::assembler::str_regex(graph::tensor<string> const &a, std::list<graph::tensor<string>> const &outputs, string const &regex)
{
	emit(opcode::str_regex, get_index(a), alloc(regex), outputs.size());
	std::for_each(outputs.begin(), outputs.end(), [this](auto const &t) { emit(get_index(t)); });
}

void compile::assembler::str_concat(graph::tensor<string> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b)
{
	emit(opcode::str_concat, get_index(x), get_index(a), get_index(b));
}

void compile::assembler::str_toupper(graph::tensor<string> const &x, graph::tensor<string> const &a)
{
	emit(opcode::str_toupper, get_index(x), get_index(a));
}

void compile::assembler::str_tonumber(graph::tensor<number> const &x, graph::tensor<string> const &a)
{
	emit(opcode::str_tonumber, get_index(x), get_index(a));
}
