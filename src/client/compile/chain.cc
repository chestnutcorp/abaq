/*!
 * @author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * @since Sep 2, 2020
 */

#include "abaq/graph/functor.h"

#include "chain.h"

#include <algorithm>

using namespace abaq;


compile::chain::chain(graph::tensor<void> const &t) : links_(1, t)
{
}

compile::chain::chain(graph::tensor<void> const &t, chain &other)
{
	/*
	 * other: tn, ..., ti, t, ..., t0 -> tn, ..., ti
	 * this: t, ..., t0
	 */
	auto it = std::find_if(other.links_.begin(), other.links_.end(), [&t](auto const &x) { return t.get_functor() == x.get_functor(); });
	links_.splice(links_.end(), other.links_, it, other.links_.end());
}

compile::chain::chain(graph::tensor<void> const &t, chain::iterator ci, chain::list &chains) : links_(std::move(ci->links_))
{
	links_.push_front(t);
	chains.erase(ci);
}

bool compile::chain::contains(graph::functor_base * f) const
{
	return std::any_of(links_.begin(), links_.end(), [f](auto const &t) { return f == t.get_functor(); });
}

bool compile::chain::can_link(graph::tensor<void> const &t)
{
	auto f = get_tail_functor();
	return f->is_single_output() && f->type() == t.type() && f->get_shape() == t.get_shape();
}
