/*!
 * @author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * @since Sep 2, 2020
 */

#include "abaq/graph/functor.h"

#include "scheduler.h"

using namespace abaq;


void compile::scheduler::process(graph::tensor<void> const &t)
{
	auto it = find_chain(t);
	if (it == chains_.end())
	{
		chains_.push_back(weave(t));
	}
	else
	{
		if (it->get_tail_functor() != t.get_functor())
			split_chain(it, t);
	}
}

compile::chain compile::scheduler::weave(graph::tensor<void> const &t)
{
	// every tensor is an output of a functor, which may have multiple inputs, so that when we iterate over all
	// input tensors and recursively weave them, we produce a list of chains - one for every input tensor.
	chain::list chains;

	// `ci` iterator points to a chain in `chains` that is most suitable to link current tensor with.
	auto ci = chains.end();

	// iterate over all parent functor inputs and produce chains for every of them
	for (auto const &x : t.get_functor()->get_inputs())
	{
		auto it = find_chain(x);
		if (it == chains_.end())
		{
			auto cj = chains.insert(chains.end(), weave(x));
			if (cj->can_link(t))
			{
				if (ci == chains.end() || ci->size() < cj->size())
					ci = cj;
			}
		}
		else
		{
			if (it->get_tail_functor() != x.get_functor())
				split_chain(it, t);
		}
	}

	if (ci != chains.end())
	{
		chain c(t, ci, chains);
		chains_.insert(chains_.end(), chains.begin(), chains.end());
		return c;
	}
	else
	{
		chains_.insert(chains_.end(), chains.begin(), chains.end());
		return chain(t);
	}
}

void compile::scheduler::split_chain(chain::iterator ci, graph::tensor<void> const &t)
{
	chains_.emplace(ci, t, *ci);
}

compile::chain::iterator compile::scheduler::find_chain(graph::tensor<void> const &t)
{
	return std::find_if(chains_.begin(), chains_.end(), [f = t.get_functor()](auto const &c) { return c.contains(f); });
}
