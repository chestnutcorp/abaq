/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 17, 2021
 */
#include "abaq/compile/assembler.h"
#include "abaq/compile/unit.h"

#include "scheduler.h"

using namespace abaq;

void compile::unit::build(graph::inputs<void> const &vars)
{
	// remember captures
	vars_.insert(vars_.end(), vars.begin(), vars.end());

	// split graph onto chains
	scheduler s;
	std::for_each(vars.begin(), vars.end(), [&s](graph::tensor<void> const &t) {
		s.process(t);
	});

	// perform tensor aliasing
	std::for_each(s.begin(), s.end(), [this](chain const &c) {
		// register all output tensors
		auto out = c.get_tail_functor()->get_outputs();
		std::for_each(out.begin(), out.end(), [this](graph::tensor<void> const &x) { get_index(x); });

		// map intermediate tensors to trunk
		auto index = get_index(c.get_links().front());
		std::for_each(c.get_links().begin(), c.get_links().end(), [this, index](graph::tensor<void> const &x) { set_index(x, index); });
	});

	// generate code
	std::for_each(s.begin(), s.end(), [this](chain const &c) {
//std::clog << "chain: " << c.size() << std::endl;
		assembler as(this);
		std::for_each(c.get_links().rbegin(), c.get_links().rend(), [&as](graph::tensor<void> const &x) {
			x.get_functor()->compile(as);
		});
	});
}

void compile::unit::deploy(compute::context::ptr const &ctx)
{
	// deploy segments
	ctx->deploy(cseg_, aseg_, nseg_, sseg_);

	// allocate membufs
	for (auto it = allocs_.begin(); it != allocs_.end(); ++it)
	{
		compute::dimension::list dims;
		for (auto const &d : it->first.get_functor()->get_shape().dims())
			dims.emplace_back(d.id(), d.size());

		ctx->alloc_membuf(dims, it->second, it->first.type());
	}

	// register captures
	std::for_each(vars_.begin(), vars_.end(), [this, &ctx](auto const &t) { ctx->capture(get_index(t)); });
}

void compile::unit::set_index(graph::tensor<void> const &t, size_t index)
{
	allocs_.emplace(t, index);
}

size_t compile::unit::get_index(graph::tensor<void> const &t)
{
	auto it = allocs_.find(t);
	if (it == allocs_.end())
		it = allocs_.emplace(t, allocs_top_++).first;
	return it->second;
}
