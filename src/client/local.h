/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 21, 2021
 */
#pragma once

#include "abaq/compute/binder.h"
#include "abaq/compute/context.h"
#include "abaq/compute/engine.h"

namespace abaq
{
	namespace compute
	{
		class local_engine : public engine
		{
		public:
			virtual context::ptr create_context() override;
		};

		class local_context : public context
		{
			cseg cs_;
			aseg as_;
			nseg ns_;
			sseg ss_;

			std::vector<membuf::ptr> bufs_;

		public:
			virtual void deploy(cseg const &cs, aseg const &as, nseg const &ns, sseg const &ss) override;

			virtual void alloc_membuf(dimension::list const &dims, size_t index, value_type type) override;

			virtual void override_dims(dimension::list const &dims) override;

			virtual void capture(size_t index) override;

			virtual void eval(binder::map const &binds) override;

			virtual membuf::ptr get_membuf(size_t index) override;

			virtual cseg const &get_cseg() override;

			virtual nseg const &get_nseg() override;

			virtual sseg const &get_sseg() override;
		};
	}
}
