/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 21, 2021
 */

#include "local.h"

#include "abaq/compute/opcode.h"
#include "abaq/compute/vm.h"

#include <algorithm>
#include <memory>
#include <optional>

using namespace abaq;

static compute::engine::registry registry(
	"local",
	"Compute engine that runs locally. Most simple reference implementation.",
	[](std::string_view const &spec) -> std::optional<compute::engine::ptr> {
		if (spec == "local")
			return std::make_shared<compute::local_engine>();
		else
			return {};
	}
);

compute::context::ptr compute::local_engine::create_context()
{
	return std::make_shared<local_context>();
}

void compute::local_context::deploy(cseg const &cs, aseg const &as, nseg const &ns, sseg const &ss)
{
	cs_ = cs;
	as_ = as;
	ns_ = ns;
	ss_ = ss;
}

void compute::local_context::alloc_membuf(dimension::list const &dims, size_t index, value_type type)
{
	if (bufs_.size() <= index)
		bufs_.resize(index + 1);

	switch (type)
	{
	case value_type::number: bufs_[index] = std::make_shared<num_membuf>(dims); break;
	case value_type::string: bufs_[index] = std::make_shared<str_membuf>(dims); break;
	}
}

void compute::local_context::override_dims(dimension::list const &dims)
{
	std::for_each(bufs_.begin(), bufs_.end(), [&dims](auto &b) { b->override_dims(dims); });
}

void compute::local_context::capture(size_t index)
{
	//TODO: implement me
	(void) index;
}

void compute::local_context::eval(binder::map const &binds)
{
	vm machine(this);
	machine.dump();

	std::for_each(as_.begin(), as_.end(), [this, &binds](size_t pc) {
		switch (static_cast<opcode>(cs_[pc]))
		{
		case opcode::num_bind:
			{
				auto it = binds.find(cs_[pc + 1]);
				if (it == binds.end())
					throw std::runtime_error("Binding is not provided.");

				auto x = get_membuf(cs_[pc + 1]);
				it->second->bind(x, cs_.data() + pc + 3, this);
			}
			break;

		case opcode::str_bind:
		default: ;
		}
	});

	machine.run();
}

compute::membuf::ptr compute::local_context::get_membuf(size_t index)
{
	return bufs_.at(index);
}

compute::cseg const &compute::local_context::get_cseg()
{
	return cs_;
}

compute::nseg const &compute::local_context::get_nseg()
{
	return ns_;
}

compute::sseg const &compute::local_context::get_sseg()
{
	return ss_;
}
