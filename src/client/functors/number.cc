/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */

#include "abaq/functors/number.h"

using namespace abaq;


void functors::range::compile(compile::assembler &as)
{
	as.num_range(get_single_result(), start_, step_);
}

graph::shape functors::range::create_shape(number start, number end, number step)
{
	auto size = static_cast<size_t>((end - start) / step);
	return graph::shape(graph::dimension(size));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void functors::floor_op::compile(compile::assembler &as)
{
	as.num_floor(get_single_result(), get_inputs().front());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

functors::reduce_op::reduce_op(graph::factory *f, const graph::tensor<number> &t, const graph::shape &s, cmd c) :
	functor_impl(f, t.get_shape() - s), cmd_(c)
{
	this->attach<number>(t, t.get_shape());
}

void functors::reduce_op::compile(compile::assembler &as)
{
	switch (cmd_)
	{
	case ADD: as.num_reduce_add(get_single_result(), get_inputs().front()); break;
	case AND: as.num_reduce_and(get_single_result(), get_inputs().front()); break;
	case OR : as.num_reduce_or(get_single_result(), get_inputs().front()); break;
	}
}
