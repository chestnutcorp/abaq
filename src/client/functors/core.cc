/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Sep 7, 2020
 */
#include "abaq/functors/core.h"

using namespace abaq;

template <> bool functors::immediate<number>::is_immediate_number(number &v) const
{
	v = imm_;
	return true;
}

template <> bool functors::immediate<number>::is_immediate_string(string &v) const
{
	(void) v;
	return false;
}

template <> void functors::immediate<number>::compile(compile::assembler &as)
{
	as.num_immediate(get_single_result(), imm_);
}


template <> bool functors::immediate<string>::is_immediate_number(number &v) const
{
	(void) v;
	return false;
}

template <> bool functors::immediate<string>::is_immediate_string(string &v) const
{
	v = imm_;
	return true;
}

template <> void functors::immediate<string>::compile(compile::assembler &as)
{
	as.str_literal(get_single_result(), imm_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <> void functors::pack<number>::compile(compile::assembler &as)
{
	graph::inputs<number> inputs(get_inputs().begin(), get_inputs().end());
	as.num_pack(get_single_result(), inputs);
}

template <> void functors::pack<string>::compile(compile::assembler &as)
{
	graph::inputs<string> inputs(get_inputs().begin(), get_inputs().end());
	as.str_pack(get_single_result(), inputs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <> void functors::bind<number>::compile(compile::assembler &as)
{
	as.num_bind(get_single_result(), dims_);
}

template <> void functors::bind<string>::compile(compile::assembler &as)
{
	as.str_bind(get_single_result(), dims_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <> void functors::list<number>::compile(compile::assembler &as)
{
	as.num_list(get_single_result(), items_);
}

template <> void functors::list<string>::compile(compile::assembler &as)
{
	as.str_list(get_single_result(), items_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <> void functors::iif<number>::compile(compile::assembler &as)
{
	auto a = get_inputs().begin();
	auto b = std::next(a);
	auto c = std::next(b);

	as.num_iif(get_single_result(), *a, *b, *c);
}

template <> void functors::iif<string>::compile(compile::assembler &as)
{
	auto a = get_inputs().begin();
	auto b = std::next(a);
	auto c = std::next(b);

	as.str_iif(get_single_result(), *a, *b, *c);
}
