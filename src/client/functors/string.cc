/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 11, 2020
 */

#include "abaq/functors/string.h"

using namespace abaq;

void functors::tonumber::compile(compile::assembler &as)
{
	as.str_tonumber(get_single_result(), get_inputs().front());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void functors::toupper::compile(compile::assembler &as)
{
	as.str_toupper(get_single_result(), get_inputs().front());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

functors::regex::regex(graph::factory *f, const graph::tensor<string> &a, string pattern, size_t nmatches) : functor_impl(f, a.get_shape()), pattern_(pattern), nmatches_(nmatches)
{
	this->attach(a);
}

graph::outputs<string> functors::regex::get_result()
{
	graph::outputs<string> res;
	for (size_t i = 0; i < nmatches_; ++i)
		res.emplace_back(this, (int) i);
	return res;
}

void functors::regex::compile(compile::assembler &as)
{
	as.str_regex(get_inputs().front(), get_result(), pattern_);
}

