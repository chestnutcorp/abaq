/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 20, 2023
 */
#include "abaq/compute/dimension.h"
#include "abaq/abaq.h"

using namespace abaq;

void compute::dimension::set_size(size_t size)
{
	if (size_ == 0 || size_ == size)
		size_ = size;
	else
		throw dimension_exception("Dimension size cannot be changed.");
}
