/*!
 * \author Yuri Plaksyik <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 7, 2021
 */
#include "abaq/compute/indexer.h"
#include "abaq/compute/opcode.h"
#include "abaq/compute/vm.h"

#include <cmath>
#include <iostream>
#include <stdexcept>

using namespace abaq;

void compute::vm::run()
{
	const auto code = ctx_->get_cseg().data();
	const auto size = ctx_->get_cseg().size();

	for (pc_ = code; pc_ < code + size; )
	{
		switch (static_cast<opcode>(*pc_))
		{
		case opcode::num_reshape:            num_reshape(); break;
		case opcode::num_immediate:          num_immediate(); break;
		case opcode::num_pack:               num_pack(); break;
		case opcode::num_bind:               num_bind(); break;
		case opcode::num_list:               num_list(); break;
		case opcode::num_range:              num_range(); break;
		case opcode::num_iif:                num_iif(); break;
		case opcode::num_add:
		case opcode::num_sub:
		case opcode::num_mul:
		case opcode::num_div:
		case opcode::num_rem:
		case opcode::num_and:
		case opcode::num_or:
		case opcode::num_xor:
		case opcode::num_lt:
		case opcode::num_le:
		case opcode::num_eq:
		case opcode::num_ne:
		case opcode::num_gt:
		case opcode::num_ge:                 num_n2n(); break;
		case opcode::num_imm_add:
		case opcode::num_imm_sub:
		case opcode::num_imm_mul:
		case opcode::num_imm_div:
		case opcode::num_imm_lt:
		case opcode::num_imm_le:
		case opcode::num_imm_eq:
		case opcode::num_imm_ne:
		case opcode::num_imm_gt:
		case opcode::num_imm_ge:             num_n1ni(); break;
		case opcode::num_floor:              num_n1n(); break;
		case opcode::num_reduce_add:
		case opcode::num_reduce_and:
		case opcode::num_reduce_or:          num_reduce(); break;
		case opcode::num_serial_parse:       num_n1sl(); break;
		case opcode::num_serial_format:      num_s1nl(); break;
		case opcode::num_serial_pack:        num_serial_pack(); break;
		case opcode::num_serial_unpack:      num_serial_unpack(); break;

		case opcode::str_reshape:            str_reshape(); break;
		case opcode::str_literal:            str_literal(); break;
		case opcode::str_pack:               str_pack(); break;
		case opcode::str_bind:               str_bind(); break;
		case opcode::str_list:               str_list(); break;
		case opcode::str_iif:                str_iif(); break;
		case opcode::str_lt:
		case opcode::str_le:
		case opcode::str_eq:
		case opcode::str_ne:
		case opcode::str_gt:
		case opcode::str_ge:                 str_n2s(); break;
		case opcode::str_imm_lt:
		case opcode::str_imm_le:
		case opcode::str_imm_eq:
		case opcode::str_imm_ne:
		case opcode::str_imm_gt:
		case opcode::str_imm_ge:             str_n1sl(); break;
		case opcode::str_regex:              str_regex(); break;
		case opcode::str_concat:             str_s2s(); break;
		case opcode::str_toupper:            str_s1s(); break;
		case opcode::str_tonumber:           str_n1s(); break;
		}
	}
}

void compute::vm::dump()
{
	auto const code = ctx_->get_cseg().data();
	auto const size = ctx_->get_cseg().size();

	for (auto pc = code; pc < code + size; )
		output(std::cout, pc);
}

void compute::vm::num_reshape()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->num_data();
	auto pa = a->num_data();

	indexer xi(x->dimensions());
	indexer ai(a->dimensions());

	for (size_t index = 0, count = x->size(); index < count; ++index)
	{
		xi.set_index(index);
		ai.fit(xi);

		px[index] = pa[ai.get_index()];
	}

	pc_ += 3;
}

void compute::vm::num_immediate()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto n = ctx_->get_nseg();

	auto px = x->num_data();
	auto im = n.at(pc_[2]);
	
	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = im;

	pc_ += 3;
}

void compute::vm::num_pack()
{
	//TODO
}

void compute::vm::num_bind()
{
	auto n = pc_[2];
	pc_ += 3 + n;
}

void compute::vm::num_list()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto n = ctx_->get_nseg();

	auto px = x->num_data();
	auto pn = n.data() + pc_[2];
	
	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = pn[index];

	pc_ += 3;
}

void compute::vm::num_range()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto n = ctx_->get_nseg();

	auto px = x->num_data();
	auto n0 = n.at(pc_[2]);
	auto n1 = n.at(pc_[3]);
	
	for (size_t index = 0, count = x->size(); index < count; ++index, n0 += n1)
		px[index] = n0;

	pc_ += 4;
}

void compute::vm::num_reduce()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->num_data();
	auto pa = a->num_data();

	indexer xi(x->dimensions());
	indexer ai(a->dimensions());

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::num_reduce_add:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = 0.0;

		for (size_t index = 0, count = a->size(); index < count; ++index)
		{
			ai.set_index(index);
			xi.fit(ai);

			px[xi.get_index()] += pa[index];
		}
		break;

	case opcode::num_reduce_and:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = True;

		for (size_t index = 0, count = a->size(); index < count; ++index)
		{
			ai.set_index(index);
			xi.fit(ai);

			if (to_bool(px[xi.get_index()]))
				px[xi.get_index()] = to_bool(pa[index]) ? True : False;
		}
		break;

	case opcode::num_reduce_or:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = False;

		for (size_t index = 0, count = a->size(); index < count; ++index)
		{
			ai.set_index(index);
			xi.fit(ai);

			if (!to_bool(px[xi.get_index()]))
				px[xi.get_index()] = to_bool(pa[index]) ? True : False;
		}
		break;

	default: ;
	}

	pc_ += 3;
}

void compute::vm::num_iif()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto b = ctx_->get_membuf(pc_[3]);
	auto c = ctx_->get_membuf(pc_[4]);

	auto px = x->num_data();
	auto pa = a->num_data();
	auto pb = b->num_data();
	auto pc = c->num_data();

	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = to_bool(pa[index]) ? pb[index] : pc[index];

	pc_ += 5;
}

void compute::vm::num_serial_pack()
{
	//TODO
}

void compute::vm::num_serial_unpack()
{
	//TODO
}

void compute::vm::str_reshape()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->str_data();
	auto pa = a->str_data();

	indexer xi(x->dimensions());
	indexer ai(a->dimensions());

	for (size_t index = 0, count = x->size(); index < count; ++index)
	{
		xi.set_index(index);
		ai.fit(xi);

		px[index] = pa[ai.get_index()];
	}

	pc_ += 3;
}

void compute::vm::str_literal()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto s = ctx_->get_sseg();

	auto px = x->str_data();
	auto li = s.at(pc_[2]);
	
	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = li;

	pc_ += 3;
}

void compute::vm::str_pack()
{
	//TODO
}

void compute::vm::str_bind()
{
	auto n = pc_[2];
	pc_ += 3 + n;
}

void compute::vm::str_list()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto s = ctx_->get_sseg();

	auto px = x->str_data();
	auto ps = s.data() + pc_[2];
	
	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = ps[index];

	pc_ += 3;
}

void compute::vm::str_iif()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto b = ctx_->get_membuf(pc_[3]);
	auto c = ctx_->get_membuf(pc_[4]);

	auto px = x->str_data();
	auto pa = a->num_data();
	auto pb = b->str_data();
	auto pc = c->str_data();

	for (size_t index = 0, count = x->size(); index < count; ++index)
		px[index] = to_bool(pa[index]) ? pb[index] : pc[index];

	pc_ += 5;
}

void compute::vm::str_regex()
{
	//TODO
}

void compute::vm::num_n1ni()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto n = ctx_->get_nseg()[pc_[3]];

	auto px = x->num_data();
	auto pa = a->num_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::num_imm_add:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n + pa[index];
		break;

	case opcode::num_imm_sub:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n - pa[index];
		break;

	case opcode::num_imm_mul:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n * pa[index];
		break;

	case opcode::num_imm_div:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n / pa[index];
		break;

	case opcode::num_imm_lt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n < pa[index] ? True : False;
		break;

	case opcode::num_imm_le:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n <= pa[index] ? True : False;
		break;

	case opcode::num_imm_eq:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n == pa[index] ? True : False;
		break;

	case opcode::num_imm_ne:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n != pa[index] ? True : False;
		break;

	case opcode::num_imm_gt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n > pa[index] ? True : False;
		break;

	case opcode::num_imm_ge:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = n >= pa[index] ? True : False;
		break;

	default: ;
	}

	pc_ += 4;
}

void compute::vm::num_n1n()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->num_data();
	auto pa = a->num_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::num_floor:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = std::floor(pa[index]);
		break;

	default: ;
	}

	pc_ += 3;
}

void compute::vm::num_n1sl()
{
	//TODO
}

void compute::vm::num_s1nl()
{
	//TODO
}

void compute::vm::num_n2n()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto b = ctx_->get_membuf(pc_[3]);

	auto px = x->num_data();
	auto pa = a->num_data();
	auto pb = b->num_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::num_add:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] + pb[index];
		break;

	case opcode::num_sub:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] - pb[index];
		break;

	case opcode::num_mul:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] * pb[index];
		break;

	case opcode::num_div:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] / pb[index];
		break;

	case opcode::num_rem:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = std::fmod(pa[index], pb[index]);
		break;

	case opcode::num_and:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = to_bool(pa[index]) && to_bool(pb[index]) ? True : False;
		break;

	case opcode::num_or:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = to_bool(pa[index]) || to_bool(pb[index]) ? True : False;
		break;

	case opcode::num_xor:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = (to_bool(pa[index]) && !to_bool(pb[index])) || (!to_bool(pa[index]) && to_bool(pb[index])) ? True : False;
		break;

	case opcode::num_lt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] < pb[index] ? True : False;
		break;

	case opcode::num_le:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] <= pb[index] ? True : False;
		break;

	case opcode::num_eq:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] == pb[index] ? True : False;
		break;

	case opcode::num_ne:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] != pb[index] ? True : False;
		break;

	case opcode::num_gt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] > pb[index] ? True : False;
		break;

	case opcode::num_ge:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] >= pb[index] ? True : False;
		break;

	default: ;
	}

	pc_ += 4;
}

void compute::vm::str_n2s()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto b = ctx_->get_membuf(pc_[3]);

	auto px = x->num_data();
	auto pa = a->str_data();
	auto pb = b->str_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::str_lt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] < pb[index] ? True : False;
		break;

	case opcode::str_le:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] <= pb[index] ? True : False;
		break;

	case opcode::str_eq:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] == pb[index] ? True : False;
		break;

	case opcode::str_ne:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] != pb[index] ? True : False;
		break;

	case opcode::str_gt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] > pb[index] ? True : False;
		break;

	case opcode::str_ge:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = pa[index] >= pb[index] ? True : False;
		break;

	default: ;
	}

	pc_ += 4;
}

void compute::vm::str_n1sl()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto s = ctx_->get_sseg()[pc_[3]];

	auto px = x->num_data();
	auto pa = a->str_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::str_imm_lt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s < pa[index] ? True : False;
		break;

	case opcode::str_imm_le:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s <= pa[index] ? True : False;
		break;

	case opcode::str_imm_eq:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s == pa[index] ? True : False;
		break;

	case opcode::str_imm_ne:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s != pa[index] ? True : False;
		break;

	case opcode::str_imm_gt:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s > pa[index] ? True : False;
		break;

	case opcode::str_imm_ge:
		for (size_t index = 0, count = x->size(); index < count; ++index)
			px[index] = s >= pa[index] ? True : False;
		break;

	default: ;
	}

	pc_ += 4;
}

void compute::vm::str_s2s()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);
	auto b = ctx_->get_membuf(pc_[3]);

	auto px = x->str_data();
	auto pa = a->str_data();
	auto pb = b->str_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::str_concat:
		for (size_t index = 0, count = px->size(); index < count; ++index)
			px[index] = pa[index] + pb[index];
		break;

	default: ;
	}

	pc_ += 4;
}

void compute::vm::str_s1s()
{
	struct toupper { int operator () (int ch) const { return std::toupper(ch); } };

	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->str_data();
	auto pa = a->str_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::str_toupper:
		if (px == pa)
		{
			for (size_t index = 0, count = x->size(); index < count; ++index)
				std::transform(pa[index].begin(), pa[index].end(), pa[index].begin(), toupper{});
		}
		else
		{
			for (size_t index = 0, count = x->size(); index < count; ++index)
			{
				px[index].resize(0);
				px[index].reserve(pa[index].size());

				std::transform(pa[index].begin(), pa[index].end(), std::back_inserter(px[index]), toupper{});
			}
		}
		break;

	default: ;
	}

	pc_ += 3;
}

void compute::vm::str_n1s()
{
	auto x = ctx_->get_membuf(pc_[1]);
	auto a = ctx_->get_membuf(pc_[2]);

	auto px = x->num_data();
	auto pa = a->str_data();

	switch (static_cast<opcode>(*pc_))
	{
	case opcode::str_tonumber:
		for (size_t index = 0, count = x->size(); index < count; ++index)
		{
			try
			{
				px[index] = std::stod(pa[index]);
			}
			catch (...)
			{
				px[index] = NaN;
			}
		}
		break;

	default: ;
	}

	pc_ += 3;
}
