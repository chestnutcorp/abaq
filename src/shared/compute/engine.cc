/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 16, 2021
 */

#include "abaq/compute/engine.h"
#include "abaq/abaq.h"

using namespace abaq;

static compute::engine::registry * registry_head = nullptr;


compute::engine::ptr compute::engine::create(const std::string_view &spec)
{
	for (auto r = registry_head; r; r = r->link_)
	{
		auto p = r->ctor_(spec);
		if (p.has_value())
			return p.value();
	}

	throw engine_exception(spec);
}

std::list<std::pair<std::string_view, std::string_view>> compute::engine::list()
{
	std::list<std::pair<std::string_view, std::string_view>> list;

	for (auto r = registry_head; r; r = r->link_)
		list.emplace_back(r->name_, r->desc_);

	return list;
}

compute::engine::registry::registry(char const * name, char const * desc, ctor func) :
	name_(name), desc_(desc), ctor_(func), link_(registry_head)
{
	registry_head = this;
}
