/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Sep 30, 2021
 */

#include "abaq/compute/membuf.h"

#include <algorithm>
#include <numeric>

using namespace abaq;

compute::membuf::membuf(dimension::list const &dims) : dims_(dims), size_(0)
{
	update_size();
}

void compute::membuf::override_dims(dimension::list const &dims)
{
	std::for_each(dims_.begin(), dims_.end(), [&dims](auto &d) {
		auto it = std::find_if(dims.begin(), dims.end(), [&d](auto const &o) { return d.id() == o.id(); });
		if (it != dims.end()) d.set_size(it->size());
	});

	update_size();
}

void compute::membuf::update_size()
{
	size_ = std::transform_reduce(dims_.begin(), dims_.end(), 1, [](size_t a, size_t s) { return a * s; }, [](auto const &d) { return d.size(); });
}


compute::num_membuf::num_membuf(dimension::list const &dims) : membuf(dims), data_(size(), 0.0)
{
}

void compute::num_membuf::override_dims(dimension::list const &dims)
{
	membuf::override_dims(dims);

	data_.resize(size());
}

number *compute::num_membuf::num_data()
{
	return data_.data();
}


compute::str_membuf::str_membuf(dimension::list const &dims) : membuf(dims), data_(size())
{
}

void compute::str_membuf::override_dims(dimension::list const &dims)
{
	membuf::override_dims(dims);

	data_.resize(size());
}

string *compute::str_membuf::str_data()
{
	return data_.data();
}
