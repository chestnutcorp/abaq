/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 13, 2020
 */


#include <iostream>


/*
compute::membuf::membuf(const dimension::list &dims) : dims_(dims), size_(1)
{
	std::for_each(dims_.begin(), dims_.end(), [this](const auto &d) { size_ *= d.size(); });
}

compute::session::session(context *c) : context_(c)
{
	context::add_ref(context_);
}

compute::session::session(const session &other) : context_(other.context_)
{
	context::add_ref(context_);
}

compute::session::~session()
{
	context::release(context_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void compute::context::dump(const tensor<void> &t)
{
	membuf *buf = get_membuf(t);
	if (buf)
	{
		if (t.type() == value_type::number)
		{
			number *ptr = buf->num_data();

			for (size_t index = 0, count = buf->size(); index < count; ++index)
				std::cout << ptr[index] << '\t';
			std::cout << std::endl;
		}

		if (t.type() == value_type::string)
		{

			string *ptr = buf->str_data();

			for (size_t index = 0, count = buf->size(); index < count; ++index)
				std::cout << '"' << ptr[index] << '"' << '\t';
			std::cout << std::endl;
		}
	}
	else
		std::cerr << "Tensor data is not available in this context." << std::endl;
}
*/
