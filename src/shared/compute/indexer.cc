/*!
 * @author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * @since Sep 7, 2020
 */

#include "abaq/compute/indexer.h"

#include <algorithm>
#include <numeric>

using namespace abaq;

compute::indexer::indexer(dimension::list const &dims) : dims_(dims), coords_(dims_.size(), 0)
{
}

size_t compute::indexer::get_index()
{
	size_t i = 0;
	return std::transform_reduce(dims_.begin(), dims_.end(), 0, [this, &i](size_t a, size_t s) { return a * s + coords_[i++]; }, [](auto const &d) { return d.size(); });
}

void compute::indexer::set_index(size_t index)
{
	size_t i = dims_.size();
	std::transform_reduce(dims_.rbegin(), dims_.rend(), index, [this, &i](size_t a, size_t s) { coords_[--i] = a % s; return a / s; }, [](auto const &d) { return d.size(); });
}

void compute::indexer::fit(indexer const &other)
{
	std::unordered_map<size_t, size_t> map;
	{
		size_t i = 0;
		std::for_each(other.dims_.begin(), other.dims_.end(), [&other, &map, &i](auto const &d) { map.emplace(d.id(), other.coords_[i++]); });
	}

	{
		size_t i = 0;
		std::for_each(dims_.begin(), dims_.end(), [this, &map, &i](auto const &d) {
			auto it = map.find(d.id());
			coords_[i++] = it != map.end() ? it->second : 0;
		});
	}
}
