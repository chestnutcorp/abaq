/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 9, 2021
 */
#include "abaq/compute/opcode.h"

#include <string_view>
#include <tuple>
#include <unordered_map>

using namespace abaq;

std::unordered_map<compute::opcode, std::tuple<std::string_view, std::string_view>> const opcode_names = {
	{ compute::opcode::num_reshape,       { "num_reshape", "TT" } },
	{ compute::opcode::num_immediate,     { "num_immediate", "TN" } },
	{ compute::opcode::num_pack,          { "num_pack", "TL" } },
	{ compute::opcode::num_bind,          { "num_bind", "TL" } },
	{ compute::opcode::num_list,          { "num_list", "TN" } },
	{ compute::opcode::num_range,         { "num_range", "TNN" } },
	{ compute::opcode::num_iif,           { "num_iif", "TTTT" } },
	{ compute::opcode::num_add,           { "num_add", "TTT" } },
	{ compute::opcode::num_sub,           { "num_sub", "TTT" } },
	{ compute::opcode::num_mul,           { "num_mul", "TTT" } },
	{ compute::opcode::num_div,           { "num_div", "TTT" } },
	{ compute::opcode::num_rem,           { "num_rem", "TTT" } },
	{ compute::opcode::num_and,           { "num_and", "TTT" } },
	{ compute::opcode::num_or,            { "num_or", "TTT" } },
	{ compute::opcode::num_xor,           { "num_xor", "TTT" } },
	{ compute::opcode::num_lt,            { "num_lt", "TTT" } },
	{ compute::opcode::num_le,            { "num_le", "TTT" } },
	{ compute::opcode::num_eq,            { "num_eq", "TTT" } },
	{ compute::opcode::num_ne,            { "num_ne", "TTT" } },
	{ compute::opcode::num_gt,            { "num_gt", "TTT" } },
	{ compute::opcode::num_ge,            { "num_ge", "TTT" } },
	{ compute::opcode::num_imm_add,       { "num_imm_add", "TTN" } },
	{ compute::opcode::num_imm_sub,       { "num_imm_sub", "TTN" } },
	{ compute::opcode::num_imm_mul,       { "num_imm_mul", "TTN" } },
	{ compute::opcode::num_imm_div,       { "num_imm_div", "TTN" } },
	{ compute::opcode::num_imm_lt,        { "num_imm_lt", "TTN" } },
	{ compute::opcode::num_imm_le,        { "num_imm_le", "TTN" } },
	{ compute::opcode::num_imm_eq,        { "num_imm_eq", "TTN" } },
	{ compute::opcode::num_imm_ne,        { "num_imm_ne", "TTN" } },
	{ compute::opcode::num_imm_gt,        { "num_imm_gt", "TTN" } },
	{ compute::opcode::num_imm_ge,        { "num_imm_ge", "TTN" } },
	{ compute::opcode::num_floor,         { "num_floor", "TT" } },
	{ compute::opcode::num_reduce_add,    { "num_reduce_add", "TT" } },
	{ compute::opcode::num_reduce_and,    { "num_reduce_and", "TT" } },
	{ compute::opcode::num_reduce_or,     { "num_reduce_or", "TT" } },
	{ compute::opcode::num_serial_parse,  { "num_serial_parse", "TTS" } },
	{ compute::opcode::num_serial_format, { "num_serial_format", "TTS" } },
	{ compute::opcode::num_serial_pack,   { "num_serial_pack", "TTTTTTTT" } },
	{ compute::opcode::num_serial_unpack, { "num_serial_unpack", "TTTTTTTT" } },
	{ compute::opcode::str_reshape,       { "str_reshape", "TT" } },
	{ compute::opcode::str_literal,       { "str_literal", "TS" } },
	{ compute::opcode::str_pack,          { "str_pack,", "TL" } },
	{ compute::opcode::str_bind,          { "str_bind", "TL" } },
	{ compute::opcode::str_list,          { "str_list", "TS" } },
	{ compute::opcode::str_iif,           { "str_iif", "TTTT" } },
	{ compute::opcode::str_lt,            { "str_lt", "TTT" } },
	{ compute::opcode::str_le,            { "str_le", "TTT" } },
	{ compute::opcode::str_eq,            { "str_eq", "TTT" } },
	{ compute::opcode::str_ne,            { "str_ne", "TTT" } },
	{ compute::opcode::str_gt,            { "str_gt", "TTT" } },
	{ compute::opcode::str_ge,            { "str_ge", "TTT" } },
	{ compute::opcode::str_imm_lt,        { "str_imm_lt", "TTS" } },
	{ compute::opcode::str_imm_le,        { "str_imm_le", "TTS" } },
	{ compute::opcode::str_imm_eq,        { "str_imm_eq", "TTS" } },
	{ compute::opcode::str_imm_ne,        { "str_imm_ne", "TTS" } },
	{ compute::opcode::str_imm_gt,        { "str_imm_gt", "TTS" } },
	{ compute::opcode::str_imm_ge,        { "str_imm_ge", "TTS" } },
	{ compute::opcode::str_regex,         { "str_regex", "TSL" } },
	{ compute::opcode::str_concat,        { "str_concat", "TTT" } },
	{ compute::opcode::str_toupper,       { "str_toupper", "TT" } },
	{ compute::opcode::str_tonumber,      { "str_tonumber", "TT" } },
};

void compute::output(std::ostream &os, cslot const * &pc)
{
	auto it = opcode_names.find(static_cast<opcode>(*pc++));
	if (it != opcode_names.end())
	{
		os << std::get<0>(it->second);

		for (size_t index = 0, count = std::get<1>(it->second).size(); index < count; ++index)
		{
			os << (index ? ", " : " ");

			auto spec = std::get<1>(it->second)[index];
			if (spec == 'T' || spec == 'N' || spec == 'S')
			{
				os << spec << *pc++;
			}
			else if (spec == 'L')
			{
				size_t length = *pc++;

				os << length;

				while (length-- > 0)
					os << ", " << *pc++;
			}
		}

	}
	else
		os << "<unknown>";

	os << std::endl;
}
