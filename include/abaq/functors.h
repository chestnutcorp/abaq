/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */
#pragma once

#include "abaq/compile/assembler.h"
#include "abaq/graph/allocator.h"
#include "abaq/graph/tensor.h"
#include "abaq/graph/functor.h"
#include "abaq/graph/factory.h"

namespace abaq
{
	namespace functors
	{
		template <typename T> class reshape_op;

		template <typename T> class functor_impl : public graph::functor<T>
		{
			graph::inputs<void> inputs_;

		public:
			functor_impl(graph::factory * f, graph::shape const &s) : graph::functor<T>(f, s) { }

			virtual graph::inputs<void> const &get_inputs() const override { return inputs_; }

			virtual graph::outputs<T> get_result() override { return graph::outputs<T>{ this }; }

		protected:
			template <typename V> void attach(graph::tensor<V> const &t)
			{
				attach<V>(t, this->get_shape());
			}

			template <typename V> void attach(graph::tensor<V> const &t, graph::shape const &s)
			{
				if (t.get_shape() != s)
				{
					auto f = t.get_functor()->get_factory()->template create<reshape_op<V>>(t, s);
					inputs_.emplace_back(f->get_result().front());
				}
				else
					inputs_.emplace_back(t);
			}
		};

		template <typename T> class reshape_op : public functor_impl<T>
		{
		public:
			reshape_op(graph::factory * f, graph::tensor<T> const &input, graph::shape const &s) : functor_impl<T>(f, s) { this->attach(input, input.get_shape()); }

			virtual void compile(compile::assembler &as) override;
		};

		template <> inline void reshape_op<number>::compile(compile::assembler &as)
		{
			as.num_reshape(get_result().front(), get_inputs().back());
		}

		template <> inline void reshape_op<string>::compile(compile::assembler &as)
		{
			as.str_reshape(get_result().front(), get_inputs().back());
		}


		template <typename T> class source_op : public functor_impl<T>
		{
		public:
			source_op(graph::factory * f) : functor_impl<T>(f, graph::shape()) { }

			source_op(graph::factory * f, graph::shape const &s) : functor_impl<T>(f, s) { }
		};


		template <typename T, typename V> class unary_op : public functor_impl<T>
		{
		public:
			unary_op(graph::factory * f, graph::tensor<V> const &a) : functor_impl<T>(f, a.get_shape())
			{
				this->attach(a);
			}
		};


		template <typename T, typename V, typename K> class binary_op : public functor_impl<T>
		{
		public:
			binary_op(graph::factory * f, graph::tensor<V> const &a, graph::tensor<V> const &b) : functor_impl<T>(f, a.get_shape() + b.get_shape())
			{
				this->attach(a);
				this->attach(b);
			}

			virtual void compile(compile::assembler &as) override
			{
				auto a = this->get_inputs().begin();
				auto b = std::next(a);

				K{ { this->get_single_result(), *a, *b } }(as);
			}
		};


		template <typename T> struct identity { T operator () (const T &x) { return x; } };

		template <typename T> struct is_immediate_value;
		template <> struct is_immediate_value<number> { bool operator () (graph::functor_base *f, number &v) { return f->is_immediate_number(v); } };
		template <> struct is_immediate_value<string> { bool operator () (graph::functor_base *f, string &v) { return f->is_immediate_string(v); } };

		template <typename T, typename V, typename K, typename K1, typename K2, typename Z1 = identity<V>, typename Z2 = identity<V>>
		class imm_binary_op : public functor_impl<T>
		{
			int case_;
			V imm_;

		public:
			imm_binary_op(graph::factory * f, graph::tensor<V> const &a, graph::tensor<V> const &b) : functor_impl<T>(f, a.get_shape() + b.get_shape())
			{
				if (is_immediate_value<V>{}(b.get_functor(), imm_))
				{
					this->attach(a);
					case_ = 1;
				}
				else if (is_immediate_value<V>{}(a.get_functor(), imm_))
				{
					this->attach(b);
					case_ = 2;
				}
				else
				{
					this->attach(a);
					this->attach(b);
					case_ = 0;
				}
			}

			virtual void compile(compile::assembler &as) override
			{
				switch (case_)
				{
				case 1: K1{ { this->get_single_result(), this->get_inputs().front(), Z1{}(imm_) } }(as); break;
				case 2: K2{ { this->get_single_result(), this->get_inputs().front(), Z2{}(imm_) } }(as); break;
				default: K{ { this->get_single_result(), this->get_inputs().front(), this->get_inputs().back() } }(as);
				}
			}
		};
	}
}
