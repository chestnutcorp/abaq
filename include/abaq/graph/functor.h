/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 11, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/graph/allocator.h"
#include "abaq/graph/shape.h"

#include <atomic>

namespace abaq
{
	namespace compile
	{
		class assembler;
	}

	namespace graph
	{
		template <typename T> class tensor;

		template <typename T> using inputs = std::list<tensor<T>>;

		template <typename T> using outputs = std::list<tensor<T>>;

		class factory;


		/*!
		 * \brief Base functor class.
		 */
		class functor_base
		{
			friend class tensor_base;

			std::atomic_size_t ref_count_;
			factory * const factory_;
			shape shape_;

		private:
			size_t add_ref() { return ++ref_count_; }

			size_t release() { return --ref_count_; }

		public:
			/*!
			 * \brief Constructs functor base.
			 *
			 * \param f factory reference the functor has been created by
			 * \param s functor result shape
			 */
			functor_base(factory * f, shape const &s);

			/*! \brief Destructor.
			 */
			virtual ~functor_base();

			/*! \brief Hash function.
			 */
			virtual size_t hash_code() const;

			/*! \brief Equal function.
			 */
			virtual bool equal_to(functor_base const * f) const;

			/*! \brief Gets factory reference.
			 */
			factory * get_factory() const { return factory_; }

			/*! \brief Gets functor result shape.
			 */
			shape const &get_shape() const { return shape_; }

			/*! \brief Gets functor value type.
			 */
			virtual value_type type() const = 0;

			/*! \brief Gets functor inputs.
			 */
			virtual inputs<void> const &get_inputs() const = 0;

			/*! \brief Gets functor outputs.
			 */
			virtual outputs<void> get_outputs() = 0;

			/*! \brief Checks if the functor produces a single result.
			 */
			virtual bool is_single_output();

			/*! \brief Checks whether the functor represents an immediate numeric value.
			 *
			 * @param val reference to a value that will store immediate value
			 */
			virtual bool is_immediate_number(number &) const { return false; }

			virtual bool is_immediate_string(string &) const { return false; }

			virtual void compile(compile::assembler &as) = 0;
		};


		/*!
		 * \brief Typed functor.
		 *
		 * It is assumed that every functor may return results of a single specific type.
		 * So, typed functor class defines a result type.
		 */
		template <typename V> class functor : public functor_base
		{
		public:
			/*! \brief Constructs functor.
			 */
			functor(factory * f, shape const &s) : functor_base(f, s) { }

			/*! \brief Retuns functor result type.
			 */
			virtual value_type type() const override { return type_of<V>(); }

			/*! \brief Gets functor outputs.
			 *
			 * Returns outputs based on type-specific results.
			 */
			virtual outputs<void> get_outputs() override
			{
				outputs<V> r(get_result());
				return outputs<void>(r.begin(), r.end());
			}

			/*! \brief Gets typed results.
			 */
			virtual outputs<V> get_result() = 0;

			tensor<V> get_single_result() { return get_result().front(); }
		};
	}
}
