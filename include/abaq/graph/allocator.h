/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 9, 2021
 */
#pragma once

#include "abaq/abaq.h"

namespace abaq
{
	namespace graph
	{
		template <typename T> class tensor;

		struct allocator
		{
			virtual size_t get_index(tensor<void> const &t) = 0;
		};
	}
}
