/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 9, 2021
 */
#pragma once

#include "abaq/compute/binder.h"
#include "abaq/compute/engine.h"
#include "abaq/compile/unit.h"
#include "abaq/graph/allocator.h"
#include "abaq/graph/functor.h"
#include "abaq/graph/shape.h"
#include "abaq/graph/tensor.h"

#include <unordered_map>

namespace abaq
{
	namespace graph
	{
		class capture //: allocator
		{
			compute::context::ptr ctx_;
			compile::unit unit_;

			std::unordered_map<tensor<void>, size_t, tensor<void>::hash> vars_allocs_;

		public:
			capture(compute::engine::ptr const &e, inputs<void> const &vars);

			void eval(compute::binder::map const &binds);

			size_t get_index(graph::tensor<void> const &t) { return unit_.get_index(t); }

			compute::membuf::ptr get_membuf(tensor<void> const &t);
		};
	}
}
