/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 11, 2021
 */
#pragma once

#include "abaq/graph/dimension.h"

namespace abaq
{
	namespace graph
	{
		template <typename T> class tensor;


		/*!
		 * \brief A tensor shape.
		 *
		 * A tensor shape is composed by a set of dimensions.
		 */
		class shape
		{
			friend bool operator == (shape const &lhs, shape const &rhs);
			friend bool operator != (shape const &lhs, shape const &rhs);

		public:
			/*! \brief Constructs an empty shape.
			 */
			shape() = default;

			/*! \brief Copy constructor.
			 */
			shape(shape const &other) = default;

			/*! \brief Move constructor.
			 */
			shape(shape &&other) = default;

			/*! \brief Dimension list copy constructor.
			 */
			explicit shape(dimension::list const &l) : dims_(l.begin(), l.end()) { }

			/*! \brief Dimension set copy constructor.
			 */
			explicit shape(dimension::set const &s) : dims_(s) { }

			/*! \brief Dimension set move constructor.
			 */
			explicit shape(dimension::set &&s) : dims_(std::move(s)) { }

			/*! \brief Single dimension constructor.
			 */
			explicit shape(dimension const &d);

			/*! \brief Two shapes constructor.
			 *
			 * The constructed shape is a union of two provided shapes.
			 *
			 * \param s1 shape
			 * \param s2 shape
			 */
			shape(shape const &s1, shape const &s2);

			/*!\brief Shape of tensors.
			 *
			 * The constructed shape is a union of all tensors' shape specified.
			 *
			 * \param list list of tensors
			 */
			template <typename T> shape(std::list<tensor<T>> const &list)
			{
				for (auto const &t : list)
					dims_.insert(t.get_shape().begin(), t.get_shape().end());
			}

			/*! \brief Hash function.
			 */
			size_t hash_code() const;

			/*! \brief Dimension set begin iterator.
			 */
			dimension::set::const_iterator begin() const { return dims_.begin(); }

			/*! \brief Dimension set end iterator.
			 */
			dimension::set::const_iterator end() const { return dims_.end(); }

			/*! \brief Checks for empty shape.
			 */
			bool empty() const { return dims_.empty(); }

			/*! \brief Extends the shape by provided one.
			 *
			 * The shape will accommodate all dimensions of provided one.
			 *
			 * \param s the shape to extend by
			 */
			void append(shape const &s);

			/*! \brief Shape rank, i.e. number of dimensions.
			 */
			size_t rank() const { return dims_.size(); }

			size_t slice_rank() const { return 0; }

			/*! \brief Shape volume.
			 *
			 * Volume is a number of tensor elements.
			 * It is computed as a product of all dimension sizes.
			 */
			size_t volume() const;

			dimension::list dims() const;

		private:
			dimension::set dims_; //!< dimension set
		};


		inline bool operator == (shape const &lhs, shape const &rhs) { return lhs.dims_ == rhs.dims_; }
		inline bool operator != (shape const &lhs, shape const &rhs) { return lhs.dims_ != rhs.dims_; }

		shape operator + (shape const &lhs, shape const &rhs);
		shape operator - (shape const &lhs, shape const &rhs);
		shape operator * (shape const &lhs, shape const &rhs);
	}
}
