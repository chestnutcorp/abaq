/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 11, 2021
 */
#pragma once

#include "abaq/graph/functor.h"

#include <memory>
#include <unordered_set>

namespace abaq
{
	namespace graph
	{
		/*!
		 * \brief Functor factory.
		 *
		 * The factory is responsible for creation/destruction of functors and their lifecycle management.
		 * It also plays a role in computation graph reduction.
		 */
		class factory
		{
			friend class tensor_base;

			using functor_ptr = std::unique_ptr<functor_base>;

			struct hash_code
			{
				size_t operator() (functor_ptr const &f) const { return f->hash_code(); }
			};

			struct equal_to
			{
				bool operator() (functor_ptr const &lhs, functor_ptr const &rhs) const { return lhs->equal_to(rhs.get()); }
			};

		public:
			/*! \brief Gets default factory.
			 */
			static factory * get_default_factory();

			/*!
			 * \brief Constructs a functor of specified type F with applied arguments Args.
			 *
			 * If a specified functor of exactly the same type and parameters already exists in the factory, no new functor is created,
			 * but rather existing one returned. This leads to optimal computation graph generation.
			 */
			template <typename F, typename ...Args> F * create(Args... args)
			{
				// create new functor
				functor_ptr f = std::make_unique<F>(this, args...);
				//std::clog << "Created functor: " << typeid(F).name() << " at " << f.get() << std::endl;

				// try to insert it into a set of functors, if similar functor already exists,
				// the new one won't be inserted and will be still owned by local unique_ptr.
				auto r = functors_.insert(std::move(f));

				return static_cast<F *>(r.first->get());
			}

		private:
			void dispose(functor_base * f);

		private:
			std::unordered_set<functor_ptr, hash_code, equal_to> functors_;
		};



	}
}
