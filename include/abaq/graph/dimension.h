/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 11, 2021
 */
#pragma once

#include "abaq/abaq.h"

#include <atomic>
#include <compare>
#include <iostream>
#include <memory>
#include <set>
#include <shared_mutex>
#include <unordered_map>
#include <vector>

namespace abaq
{
	namespace graph
	{
		/*!
		 * \brief Shape dimension.
		 *
		 * \see shape
		 */
		class dimension
		{
			struct data
			{
				size_t id;
				size_t size;

				data(size_t id, size_t size) : id(id), size(size) { }
			};

			std::shared_ptr<data> data_;

		public:
			/// Defines dimension order based on current evaluation graph.
			struct order
			{
				bool operator () (dimension const &lhs, dimension const &rhs) const;
			};

			/*! \brief Set of dimensions defining a shape.
			 */
			using set = std::set<dimension>;

			/*! \brief Ordered list of dimensions used in data indexing.
			 */
			using list = std::vector<dimension>;


			/*! \brief Constructs new nameless dimension of unknown size.
			 */
			dimension() : dimension(0) { }

			/*! \brief Constructs new dimension of specified size.
			 *
			 * \param size dimension size
			 */
			dimension(size_t size);

			/*! \brief Copy constructor.
			 */
			dimension(dimension const &other) = default;

			/*! \brief Move constructor.
			 */
			dimension(dimension &&other) = default;

			/*! \brief Dimension unique id.
			 */
			size_t id() const { return data_->id; }

			/*! \brief dimension size.
			 *
			 * \returns dimension size or 0 if dimension size is unknown
			 */
			size_t size() const { return data_->size; }

			/*! \brief Ensure dimension size is as specified.
			 *
			 * \param size size for the dimension.
			 */
			void ensure_size(size_t size) const;

			/*! \brief Defines partial order over the dimension and a specified set of dimensions.
			 *
			 * \param dims subordinate dimensions
			 * \throw dimension_exception if it is not possible to set the order.
			 */
			void subordinate(set const &dims);

			/*! \brief Hash function.
			 */
			size_t hash_code() const;

			/*! \brief Output dimension textual presentation to specified stream.
			 */
			void output(std::ostream &os) const;

			/*! \brief Compare operator.
			 */
			auto operator <=> (dimension const &other) const { return data_.get() <=> other.data_.get(); }

			dimension &operator = (dimension const &other) = default;

		private:
			static bool set_order_locked(size_t a, size_t b);

			static std::atomic_size_t gen;

			static std::list<std::weak_ptr<data>> dimensions;
			static std::shared_mutex dimensions_mutex;

			static std::unordered_multimap<size_t, size_t> orders;
			static std::shared_mutex orders_mutex;
		};

		inline std::ostream & operator << (std::ostream &os, dimension const &d) { d.output(os); return os; }

		inline bool operator == (dimension const &lhs, dimension const &rhs) { return (lhs <=> rhs) == 0; }
		inline bool operator != (dimension const &lhs, dimension const &rhs) { return (lhs <=> rhs) != 0; }
		inline bool operator  < (dimension const &lhs, dimension const &rhs) { return (lhs <=> rhs)  < 0; }
	}
}
