/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 11, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/graph/shape.h"

namespace abaq
{
	namespace graph
	{
		class functor_base;

		template <typename T> class functor;


		/*!
		 * \brief Tensor base.
		 *
		 * Defines base tensor functionality.
		 */
		class tensor_base
		{
			friend bool operator == (const tensor_base &lhs, const tensor_base &rhs) { return lhs.functor_ == rhs.functor_ && lhs.id_ == rhs.id_; }
			friend bool operator != (const tensor_base &lhs, const tensor_base &rhs) { return lhs.functor_ != rhs.functor_ || lhs.id_ != rhs.id_; }


			functor_base * functor_;
			int id_;

		protected:
			/*!
			 * \brief Constructs tensor as a functor result.
			 *
			 * Every tensor is considered to be a result of some functor evaluation. In most cases functors return a single resulting tensor.
			 * However, in some cases functors may return multiple results. Resulting tensors are distinguished by some `id`, which doesn't hold
			 * any specific meaning, except tensor objects having the same `id` are considered to be equal, i.e. represent the same output.
			 *
			 * It is theoretically possible that a functor returns two or more tensors with the same `id`.
			 *
			 * \param f functor reference the tensor is result of
			 * \param id tensor id
			 */
			tensor_base(functor_base * f, int id);

			/*! \brief Copy constructor.
			 */
			tensor_base(tensor_base const &other);

			/*! \brief Destructor.
			 */
			~tensor_base();

			/*! \brief Assigns other tensor to this one.
			 */
			void assign(tensor_base const &other);

			/*! \brief Releases reference to the existing functor and disposes it if necessary.
			 */
			void dispose();

			/*! \brief Checks that tensor type matches specified one.
			 *
			 * If types do not match, the `type_exception` is thrown.
			 */
			void check_type(value_type t);

		public:
			
			/*! \brief Hashing function.
			 */
			struct hash { size_t operator() (tensor_base const &t) const { return t.hash_code(); } };

			/*! \brief Gets a functor the tensor is result of.
			 */
			functor_base * get_functor() const { return functor_; }

			/*! \brief Gets tensor id.
			 */
			int get_id() const { return id_; }

			/*! \brief Hash function.
			 */
			size_t hash_code() const;

			/*! \brief Tensor value type.
			 */
			value_type type() const;

			/*! \brief Gets tensor shape.
			 */
			shape const &get_shape() const;

			/*! \brief Gets tensor rank.
			 */
			size_t rank() const;

			/*! \brief Gets tensor volume.
			 */
			size_t volume() const;
		};


		/*!
		 * \brief Typed tensor.
		 */
		template <typename T> class tensor : public tensor_base
		{
		public:
			/*!
			 * \brief Constructs a tensor as a result of functor.
			 *
			 * \param f functor the tensor is result of
			 * \param id optional result id
			 */
			tensor(functor<T> * f, int id = 0) : tensor_base(f, id) { }

			/*! \brief Copy constructor.
			 */
			tensor(tensor<T> const &other) = default;

			/*! \brief Copy constructor from different type.
			 */
			template <typename E> tensor(tensor<E> const &other);

			/*! \brief Destructor.
			 */
			~tensor() { }

			/*! \brief Copy assign operator,
			 */
			tensor<T> & operator = (tensor<T> const &t) { assign(t); return *this; }

			/*! \brief Casts tensor to specified type.
			 *
			 * If it is impossible to case, `type_exception` is thrown.
			 */
			template <typename E> tensor<E> cast() const { return tensor<E>(*this); }
		};

		template <> template<typename E>
		inline tensor<void>::tensor(tensor<E> const &other) : tensor_base(other.get_functor(), other.get_id()) { }

		template <> template<>
		inline tensor<number>::tensor(tensor<void> const &other) : tensor_base(other.get_functor(), other.get_id()) { check_type(value_type::number); }

		template <> template<>
		inline tensor<string>::tensor(tensor<void> const &other) : tensor_base(other.get_functor(), other.get_id()) { check_type(value_type::string); }

		template <typename T> template<typename E>
		inline tensor<T>::tensor(tensor<E> const &other) : tensor_base(other.get_functor(), other.get_id())
		{
			static_assert(type_of<T>() == type_of<E>(), "cannot convert from string to number");
		}
	}
}
