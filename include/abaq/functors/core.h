/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */
#pragma once

#include "abaq/functors.h"

#include <functional>
#include <numeric>
#include <vector>

namespace abaq
{
	namespace functors
	{
		template <typename T> class immediate : public source_op<T>
		{
			T imm_;

		public:
			immediate(graph::factory * f, T const &v) : source_op<T>(f), imm_(v) { }

			immediate(graph::factory * f, T const &v, graph::shape const &s) : source_op<T>(f, s), imm_(v) { }

			virtual size_t hash_code() const override
			{
				return 3 * source_op<T>::hash_code() + std::hash<T>{}(imm_);
			}

			virtual bool equal_to(graph::functor_base const * f) const override
			{
				return source_op<T>::equal_to(f) && imm_ == static_cast<immediate<T> const *>(f)->imm_;
			}

			virtual bool is_immediate_number(number &v) const override;

			virtual bool is_immediate_string(string &v) const override;

			virtual void compile(compile::assembler &as) override;
		};

		// --

		template <typename T> class bind : public source_op<T>
		{
			graph::dimension::list dims_;

		public:
			bind(graph::factory * f, graph::dimension::list const &dims) : source_op<T>(f, graph::shape(dims)), dims_(dims) { }

			virtual bool equal_to(graph::functor_base const *) const override { return false; } //TODO: revise me

			virtual void compile(compile::assembler &as) override;
		};

		// --

		template <typename T> class list : public source_op<T>
		{
			std::vector<T> items_;

		public:
			list(graph::factory * f, graph::dimension const &d, std::vector<T> const &items) : source_op<T>(f, graph::shape(d)), items_(items)
			{
				d.ensure_size(items.size());
			}

			virtual size_t hash_code() const override
			{
				return std::transform_reduce(items_.begin(), items_.end(), source_op<T>::hash_code(),
						[](size_t a, size_t b) { return 3 * a + b; },   // reduce function
						[](T const &v) { return std::hash<T>{}(v); });  // transform function
			}

			virtual bool equal_to(graph::functor_base const * f) const override
			{
				return source_op<T>::equal_to(f) && items_ == static_cast<list<T> const *>(f)->items_;
			}

			virtual void compile(compile::assembler &as) override;
		};

		// --

		template <typename T> class pack : public functor_impl<T>
		{
			graph::dimension dimension_;

		public:
			pack(graph::factory * f, graph::dimension const &d, graph::inputs<T> const &inputs) : functor_impl<T>(f, graph::shape(inputs) + graph::shape(d)), dimension_(d)
			{
				dimension_.ensure_size(inputs.size());

				auto s = this->get_shape() - graph::shape(dimension_);
				for (const auto &t : inputs)
					this->attach(t, s);
			}

			virtual void compile(compile::assembler &as) override;
		};

		// --

		template <typename T> class iif : public functor_impl<T>
		{
		public:
			iif(graph::factory * f, graph::tensor<number> const &a, graph::tensor<T> const &b, graph::tensor<T> const &c) :
				functor_impl<T>(f, a.get_shape() + b.get_shape() + c.get_shape())
			{
				this->attach(a);
				this->attach(b);
				this->attach(c);
			}

			virtual void compile(compile::assembler &as) override;
		};
	}
}
