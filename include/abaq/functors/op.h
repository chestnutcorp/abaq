/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 17, 2021
 */
#pragma once

#include "abaq/compile/assembler.h"
#include "abaq/graph/tensor.h"

namespace abaq
{
	namespace functors
	{
		namespace op
		{
			template <typename T, typename V = T> struct binary { graph::tensor<T> x; graph::tensor<V> a; graph::tensor<V> b; };

			struct num_add : public binary<number> { void operator () (compile::assembler &as) { as.num_add(x, a, b); } };
			struct num_sub : public binary<number> { void operator () (compile::assembler &as) { as.num_sub(x, a, b); } };
			struct num_mul : public binary<number> { void operator () (compile::assembler &as) { as.num_mul(x, a, b); } };
			struct num_div : public binary<number> { void operator () (compile::assembler &as) { as.num_div(x, a, b); } };
			struct num_rem : public binary<number> { void operator () (compile::assembler &as) { as.num_rem(x, a, b); } };
			struct num_and : public binary<number> { void operator () (compile::assembler &as) { as.num_and(x, a, b); } };
			struct num_or  : public binary<number> { void operator () (compile::assembler &as) { as.num_or(x, a, b); } };
			struct num_xor : public binary<number> { void operator () (compile::assembler &as) { as.num_xor(x, a, b); } };

			struct num_lt : public binary<number> { void operator () (compile::assembler &as) { as.num_lt(x, a, b); } };
			struct num_le : public binary<number> { void operator () (compile::assembler &as) { as.num_le(x, a, b); } };
			struct num_eq : public binary<number> { void operator () (compile::assembler &as) { as.num_eq(x, a, b); } };
			struct num_ne : public binary<number> { void operator () (compile::assembler &as) { as.num_ne(x, a, b); } };
			struct num_gt : public binary<number> { void operator () (compile::assembler &as) { as.num_gt(x, a, b); } };
			struct num_ge : public binary<number> { void operator () (compile::assembler &as) { as.num_ge(x, a, b); } };

			struct str_lt : public binary<number, string> { void operator () (compile::assembler &as) { as.str_lt(x, a, b); } };
			struct str_le : public binary<number, string> { void operator () (compile::assembler &as) { as.str_le(x, a, b); } };
			struct str_eq : public binary<number, string> { void operator () (compile::assembler &as) { as.str_eq(x, a, b); } };
			struct str_ne : public binary<number, string> { void operator () (compile::assembler &as) { as.str_ne(x, a, b); } };
			struct str_gt : public binary<number, string> { void operator () (compile::assembler &as) { as.str_gt(x, a, b); } };
			struct str_ge : public binary<number, string> { void operator () (compile::assembler &as) { as.str_ge(x, a, b); } };

			struct str_concat : public binary<string> { void operator () (compile::assembler &as) { as.str_concat(x, a, b); } };


			template <typename T, typename V = T> struct imm_binary { graph::tensor<T> x; graph::tensor<V> a; V v; };

			struct num_imm_add : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_add(x, a, v); } };
			struct num_imm_sub : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_sub(x, a, v); } };
			struct num_imm_mul : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_mul(x, a, v); } };
			struct num_imm_div : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_div(x, a, v); } };

			struct num_imm_lt : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_lt(x, a, v); } };
			struct num_imm_le : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_le(x, a, v); } };
			struct num_imm_eq : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_eq(x, a, v); } };
			struct num_imm_ne : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_ne(x, a, v); } };
			struct num_imm_gt : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_gt(x, a, v); } };
			struct num_imm_ge : public imm_binary<number> { void operator () (compile::assembler &as) { as.num_imm_ge(x, a, v); } };

			struct str_imm_lt : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_lt(x, a, v); } };
			struct str_imm_le : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_le(x, a, v); } };
			struct str_imm_eq : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_eq(x, a, v); } };
			struct str_imm_ne : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_ne(x, a, v); } };
			struct str_imm_gt : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_gt(x, a, v); } };
			struct str_imm_ge : public imm_binary<number, string> { void operator () (compile::assembler &as) { as.str_imm_ge(x, a, v); } };
		}
	}
}
