/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */

#ifndef ABAQ_FUNCTORS_COMPARE
#define ABAQ_FUNCTORS_COMPARE

#include "abaq/functors.h"
#include "abaq/functors/op.h"

namespace abaq
{
	namespace functors
	{
		class num_lt_op : public imm_binary_op<number, number, op::num_lt, op::num_imm_gt, op::num_imm_lt>
		{
		public:
			num_lt_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class num_le_op : public imm_binary_op<number, number, op::num_le, op::num_imm_ge, op::num_imm_le>
		{
		public:
			num_le_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class num_eq_op : public imm_binary_op<number, number, op::num_eq, op::num_imm_eq, op::num_imm_eq>
		{
		public:
			num_eq_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class num_ne_op : public imm_binary_op<number, number, op::num_ne, op::num_imm_ne, op::num_imm_ne>
		{
		public:
			num_ne_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class num_gt_op : public imm_binary_op<number, number, op::num_gt, op::num_imm_lt, op::num_imm_gt>
		{
		public:
			num_gt_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class num_ge_op : public imm_binary_op<number, number, op::num_ge, op::num_imm_le, op::num_imm_ge>
		{
		public:
			num_ge_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};


		class str_lt_op : public imm_binary_op<number, string, op::str_lt, op::str_imm_gt, op::str_imm_lt>
		{
		public:
			str_lt_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};

		class str_le_op : public imm_binary_op<number, string, op::str_le, op::str_imm_ge, op::str_imm_le>
		{
		public:
			str_le_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};

		class str_eq_op : public imm_binary_op<number, string, op::str_eq, op::str_imm_eq, op::str_imm_eq>
		{
		public:
			str_eq_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};

		class str_ne_op : public imm_binary_op<number, string, op::str_ne, op::str_imm_ne, op::str_imm_ne>
		{
		public:
			str_ne_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};

		class str_gt_op : public imm_binary_op<number, string, op::str_gt, op::str_imm_lt, op::str_imm_gt>
		{
		public:
			str_gt_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};

		class str_ge_op : public imm_binary_op<number, string, op::str_ge, op::str_imm_le, op::str_imm_ge>
		{
		public:
			str_ge_op(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : imm_binary_op(f, a, b) { }
		};
	}
}

#endif

