/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */
#pragma once

#include "abaq/functors.h"

namespace abaq
{
	namespace functors
	{
		class feed_op : public functor_impl<string>
		{
		public:
			feed_op(graph::factory * f, graph::tensor<string> const &input) : functor_impl<string>(f, input.get_shape()) { this->attach(input, input.get_shape()); }

			virtual graph::outputs<string> get_result() override { return graph::outputs<string>{ this }; }

			virtual void compile(compile::assembler &as) override;
		};
	}
}
