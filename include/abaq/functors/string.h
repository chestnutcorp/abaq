/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 11, 2020
 */
#pragma once

#include "abaq/functors.h"
#include "abaq/functors/op.h"

namespace abaq
{
	namespace functors
	{
		class concat : public binary_op<string, string, op::str_concat>
		{
		public:
			concat(graph::factory *f, const graph::tensor<string> &a, const graph::tensor<string> &b) : binary_op(f, a, b) { }
		};

		class tonumber : public unary_op<number, string>
		{
		public:
			tonumber(graph::factory *f, const graph::tensor<string> &a) : unary_op(f, a) { }

			virtual void compile(compile::assembler &as) override;
		};

		class toupper : public unary_op<string, string>
		{
		public:
			toupper(graph::factory *f, const graph::tensor<string> &a) : unary_op(f, a) { }

			virtual void compile(compile::assembler &as) override;
		};

		class regex : public functor_impl<string>
		{
			string pattern_;
			size_t nmatches_;

		public:
			regex(graph::factory *f, const graph::tensor<string> &a, string pattern, size_t nmatches);

			virtual graph::outputs<string> get_result() override;

			virtual void compile(compile::assembler &as) override;
		};
	}
}
