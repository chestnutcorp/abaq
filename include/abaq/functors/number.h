/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 12, 2020
 */
#pragma once

#include "abaq/functors.h"

#include <string_view>

namespace abaq
{
	namespace functors
	{
		class range : public source_op<number>
		{
			number start_;
			number step_;

		public:
			range(graph::factory *f, number start, number end, number step) :
				source_op(f, create_shape(start, end, step)), start_(start), step_(step) { }

			virtual void compile(compile::assembler &as) override;

		private:
			static graph::shape create_shape(number start, number end, number step);
		};

		class floor_op : public unary_op<number, number>
		{
		public:
			floor_op(graph::factory *f, const graph::tensor<number> &a) : unary_op(f, a) { }

			virtual void compile(compile::assembler &as) override;
		};

		class reduce_op : public functor_impl<number>
		{
		public:
			enum cmd { ADD, AND, OR };

			reduce_op(graph::factory *f, const graph::tensor<number> &t, const graph::shape &s, cmd c);

			virtual void compile(compile::assembler &as) override;

		private:
			cmd cmd_;
		};
	}
}
