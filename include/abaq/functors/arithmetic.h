/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 10, 2020
 */

#ifndef ABAQ_FUNCTORS_ARITHMETIC
#define ABAQ_FUNCTORS_ARITHMETIC

#include "abaq/functors.h"
#include "abaq/functors/op.h"

namespace abaq
{
	namespace functors
	{
		struct negate  { number operator () (const number &v) { return -v; } };
		struct inverse { number operator () (const number &v) { return 1 / v; } };


		class add_op : public imm_binary_op<number, number, op::num_add, op::num_imm_add, op::num_imm_add>
		{
		public:
			add_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class sub_op : public imm_binary_op<number, number, op::num_sub, op::num_imm_add, op::num_imm_sub, negate>
		{
		public:
			sub_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class mul_op : public imm_binary_op<number, number, op::num_mul, op::num_imm_mul, op::num_imm_mul>
		{
		public:
			mul_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class div_op : public imm_binary_op<number, number, op::num_div, op::num_imm_mul, op::num_imm_div, inverse>
		{
		public:
			div_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : imm_binary_op(f, a, b) { }
		};

		class rem_op : public binary_op<number, number, op::num_rem>
		{
		public:
			rem_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : binary_op(f, a, b) { }
		};

		class and_op : public binary_op<number, number, op::num_and>
		{
		public:
			and_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : binary_op(f, a, b) { }
		};

		class or_op : public binary_op<number, number, op::num_or>
		{
		public:
			or_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : binary_op(f, a, b) { }
		};

		class xor_op : public binary_op<number, number, op::num_xor>
		{
		public:
			xor_op(graph::factory *f, const graph::tensor<number> &a, const graph::tensor<number> &b) : binary_op(f, a, b) { }
		};
	}
}

#endif

