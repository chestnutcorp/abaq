/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 20, 2021
 */
#pragma once

#include "abaq/abaq.h"

#include <vector>

namespace abaq
{
	namespace compute
	{
		template <typename T> using segment = std::vector<T>;

		using aslot = size_t;
		using cslot = size_t;
		using nslot = number;
		using sslot = string;

		using aseg = segment<aslot>;
		using cseg = segment<cslot>;
		using nseg = segment<nslot>;
		using sseg = segment<sslot>;
	}
}
