/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since May 10, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/compute/compute.h"

#include <ostream>

namespace abaq
{
	namespace compute
	{
		enum class opcode : size_t
		{
			/* core */
			num_reshape,              // op, x, a
			num_immediate,            // op, value
			num_pack,                 // op, x, n, a1, ..., an
			num_bind,                 // op, x, n, d1, ..., dn
			num_list,                 // op, x, index
			num_range,                // op, start, step
			num_iif,                  // op, x, a, b, c

			/* arithmetic */
			num_add,                  // op, x, a, b
			num_sub,                  // op, x, a, b
			num_mul,                  // op, x, a, b
			num_div,                  // op, x, a, b
			num_rem,                  // op, x, a, b
			num_and,                  // op, x, a, b
			num_or,                   // op, x, a, b
			num_xor,                  // op, x, a, b

			/* compare */
			num_lt,                   // op, x, a, b
			num_le,                   // op, x, a, b
			num_eq,                   // op, x, a, b
			num_ne,                   // op, x, a, b
			num_gt,                   // op, x, a, b
			num_ge,                   // op, x, a, b

			/* imm arith */
			num_imm_add,              // op, x, a, value
			num_imm_sub,              // op, x, a, value
			num_imm_mul,              // op, x, a, value
			num_imm_div,              // op, x, a, value

			/* imm comp */
			num_imm_lt,               // op, x, a, value
			num_imm_le,               // op, x, a, value
			num_imm_eq,               // op, x, a, value
			num_imm_ne,               // op, x, a, value
			num_imm_gt,               // op, x, a, value
			num_imm_ge,               // op, x, a, value

			/* math */
			num_floor,                // op, x, a
			num_reduce_add,           // op, x, a
			num_reduce_and,           // op, x, a
			num_reduce_or,            // op, x, a

			/* serial */
			num_serial_parse,         // op, x, a, format
			num_serial_format,        // op, x, a, format
			num_serial_pack,          // op, x, a0, ..., a6
			num_serial_unpack,        // op, a, x0, ..., x6

			/* core */
			str_reshape,              // op, x, a
			str_literal,              // op, x, value
			str_pack,                 // op, x, n, a1, ..., an
			str_bind,                 // op, x, n, d1, ..., dn
			str_list,                 // op, x, index
			str_iif,                  // op, x, a, b, c

			/* compare */
			str_lt,                   // op, x, a, b
			str_le,                   // op, x, a, b
			str_eq,                   // op, x, a, b
			str_ne,                   // op, x, a, b
			str_gt,                   // op, x, a, b
			str_ge,                   // op, x, a, b

			/* imm comp */
			str_imm_lt,               // op, x, a, value
			str_imm_le,               // op, x, a, value
			str_imm_eq,               // op, x, a, value
			str_imm_ne,               // op, x, a, value
			str_imm_gt,               // op, x, a, value
			str_imm_ge,               // op, x, a, value

			/* string */
			str_regex,                // op, a, regex, n, x1, ..., xn
			str_concat,               // op, x, a, b
			str_toupper,              // op, x, a
			str_tonumber,             // op, x, a
		};

		void output(std::ostream &os, cslot const * &pc);
	}
}
