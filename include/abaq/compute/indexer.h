/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Sep 7, 2020
 */
#pragma once

#include "abaq/compute/dimension.h"

#include <vector>

namespace abaq
{
	namespace compute
	{
		class indexer
		{
			dimension::list const dims_;
			std::vector<ssize_t> coords_;

		public:
			indexer(dimension::list const &dims);

			auto coords() { return coords_.data(); }

			size_t get_index();

			void set_index(size_t index);

			void fit(indexer const &other);
		};
	}
}
