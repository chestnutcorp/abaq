/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 16, 2021
 */
#pragma once

#include "abaq/compute/context.h"

#include <functional>
#include <memory>
#include <optional>
#include <string_view>

namespace abaq
{
	namespace compute
	{
		struct engine
		{
			using ptr = std::shared_ptr<engine>;
			using ctor = std::function<std::optional<ptr>(std::string_view const &)>;


			virtual ~engine() = default;

			virtual context::ptr create_context() = 0;


			static ptr create(std::string_view const &spec);

			static std::list<std::pair<std::string_view, std::string_view>> list();


			class registry
			{
				friend struct engine;

				const char * const name_;
				const char * const desc_;

				ctor const ctor_;
				registry * const link_;

			public:
				registry(char const * name, char const * desc, ctor func);
			};
		};
	}
}
