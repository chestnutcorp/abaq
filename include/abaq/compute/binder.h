/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \date May 26, 2024
 */
#pragma once

#include "abaq/compute/membuf.h"

#include <unordered_map>

namespace abaq::compute
{
	struct context;

	struct binder
	{
		using map = std::unordered_map<size_t, binder *>;


		virtual ~binder() = default;

		virtual void bind(membuf::ptr const &buf, size_t const * ids, context * ctx) = 0;
	};
}
