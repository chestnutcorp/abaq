/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 9, 2021
 */
#pragma once

#include "abaq/abaq.h"

#include <vector>

namespace abaq
{
	namespace compute
	{
		class dimension
		{
			size_t const id_;
			size_t size_;

		public:
			using list = std::vector<dimension>;


			dimension(size_t id, size_t size) : id_(id), size_(size) { }

			dimension(dimension const &) = default;

			auto id() const { return id_; }

			auto size() const { return size_; }

			void set_size(size_t size);

			bool equals(dimension const &other) const { return id_ == other.id_; }
		};

		inline bool operator == (dimension const &lhs, dimension const &rhs) { return lhs.equals(rhs); }
	}
}
