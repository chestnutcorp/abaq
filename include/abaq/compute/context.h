/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 16, 2021
 */
#pragma once

#include "abaq/compute/binder.h"
#include "abaq/compute/compute.h"
#include "abaq/compute/dimension.h"
#include "abaq/compute/membuf.h"

#include <memory>
#include <unordered_map>

namespace abaq::compute
{
	struct context
	{
		using ptr = std::shared_ptr<context>;


		virtual ~context() = default;

		virtual void deploy(cseg const &cs, aseg const &as, nseg const &ns, sseg const &ss) = 0;

		virtual void alloc_membuf(dimension::list const &dims, size_t index, value_type type) = 0;

		virtual void override_dims(dimension::list const &dims) = 0;

		virtual void capture(size_t index) = 0;

		virtual void eval(binder::map const &binds) = 0;

		virtual membuf::ptr get_membuf(size_t index) = 0;

		virtual cseg const &get_cseg() = 0;

		virtual nseg const &get_nseg() = 0;

		virtual sseg const &get_sseg() = 0;
	};
}
