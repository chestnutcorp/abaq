/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 1, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/compute/context.h"

namespace abaq
{
	namespace compute
	{
		class vm
		{
			using code_ptr = cslot const *;

			context * ctx_;

			code_ptr pc_;

		public:
			vm(context * ctx) : ctx_(ctx) { }

			void run();

			void dump();

		private:
			void num_reshape();
			void num_immediate();
			void num_pack();
			void num_bind();
			void num_list();
			void num_range();
			void num_reduce();
			void num_iif();
			void num_serial_pack();
			void num_serial_unpack();

			void str_reshape();
			void str_literal();
			void str_pack();
			void str_bind();
			void str_list();
			void str_iif();
			void str_regex();


			void num_n1ni();
			void num_n1n();
			void num_n1sl();
			void num_s1nl();
			void num_n2n();

			void str_n2s();
			void str_n1sl();
			void str_s2s();
			void str_s1s();
			void str_n1s();
		};
	}
}
