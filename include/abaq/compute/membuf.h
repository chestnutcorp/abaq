/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jun 16, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/compute/dimension.h"

#include <memory>
#include <vector>

namespace abaq
{
	namespace compute
	{
		class membuf
		{
			dimension::list dims_;
			size_t size_;

		public:
			using ptr = std::shared_ptr<membuf>;


			membuf(dimension::list const &dims);

			virtual ~membuf() = default;

			dimension::list const & dimensions() const { return dims_; }

			size_t size() const { return size_; }

			virtual void override_dims(dimension::list const &dims);

			virtual number * num_data() { throw type_exception(); }

			virtual string * str_data() { throw type_exception(); }

		private:
			void update_size();
		};

		class num_membuf : public membuf
		{
			std::vector<number> data_;

		public:
			num_membuf(dimension::list const &dims);

			virtual void override_dims(dimension::list const &dims) override;

			virtual number * num_data() override;
		};

		class str_membuf : public membuf
		{
			std::vector<string> data_;

		public:
			str_membuf(dimension::list const &dims);

			virtual void override_dims(dimension::list const &dims) override;

			virtual string * str_data() override;
		};

	}
}
