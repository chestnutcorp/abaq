/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 1, 2021
 */
#pragma once

#include "abaq/compute/opcode.h"
#include "abaq/graph/tensor.h"

#include <array>
#include <map>
#include <vector>

namespace abaq
{
	namespace compile
	{
		using serial_parts = std::array<graph::tensor<number>, 7>;

		class unit;

		class assembler
		{
			using opcode = compute::opcode;

			unit * const unit_;

		public:
			assembler(unit * u) : unit_(u) { }

		private:
			size_t alloc(number value);

			size_t alloc(string const &value);

			size_t alloc(std::vector<number> const &values);

			size_t alloc(std::vector<string> const &values);

			size_t get_index(graph::tensor<void> const &t);

			void emit(size_t i0);

			void emit(compute::opcode op, size_t i1);

			void emit(compute::opcode op, size_t i1, size_t i2);

			void emit(compute::opcode op, size_t i1, size_t i2, size_t i3);

			void emit(compute::opcode op, size_t i1, size_t i2, size_t i3, size_t i4);

			void emit(compute::opcode op, size_t i1, size_t i2, size_t i3, size_t i4, size_t i5, size_t i6, size_t i7, size_t i8);

		public:
			/* core */
			void num_reshape(graph::tensor<number> const &x, graph::tensor<number> const &a);

			void num_immediate(graph::tensor<number> const &x, number value);

			void num_range(graph::tensor<number> const &x, number start, number step);

			void num_bind(graph::tensor<number> const &x, graph::dimension::list const &dims);

			void num_list(graph::tensor<number> const &x, std::vector<number> const &values);

			void num_pack(graph::tensor<number> const &x, std::list<graph::tensor<number>> const &inputs);

			void num_iif(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b, graph::tensor<number> const &c);

			/* arithmetic */
			void num_add(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_sub(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_mul(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_div(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_rem(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_and(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_or(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_xor(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			/* imm arith */
			void num_imm_add(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_sub(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_mul(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_div(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			/* compare */
			void num_lt(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_le(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_eq(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_ne(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_gt(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			void num_ge(graph::tensor<number> const &x, graph::tensor<number> const &a, graph::tensor<number> const &b);

			/* imm comp */
			void num_imm_lt(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_le(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_eq(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_ne(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_gt(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			void num_imm_ge(graph::tensor<number> const &x, graph::tensor<number> const &a, number value);

			/* math */
			void num_floor(graph::tensor<number> const &x, graph::tensor<number> const &a);

			void num_reduce_add(graph::tensor<number> const &x, graph::tensor<number> const &a);

			void num_reduce_and(graph::tensor<number> const &x, graph::tensor<number> const &a);

			void num_reduce_or(graph::tensor<number> const &x, graph::tensor<number> const &a);

			/* serial */
			void num_serial_parse(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &format);

			void num_serial_format(graph::tensor<string> const &x, graph::tensor<number> const &a, string const &format);

			void num_serial_pack(graph::tensor<number> const &x, serial_parts const &inputs);

			void num_serial_unpack(graph::tensor<number> const &a, serial_parts const &outputs);


			/* core */
			void str_reshape(graph::tensor<string> const &x, graph::tensor<string> const &a);

			void str_literal(graph::tensor<string> const &x, string const &value);

			void str_bind(graph::tensor<string> const &x, graph::dimension::list const &dims);

			void str_list(graph::tensor<string> const &x, std::vector<string> const &values);

			void str_pack(graph::tensor<string> const &x, std::list<graph::tensor<string>> const &inputs);

			void str_iif(graph::tensor<string> const &x, graph::tensor<number> const &a, graph::tensor<string> const &b, graph::tensor<string> const &c);

			/* compare */
			void str_lt(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_le(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_eq(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_ne(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_gt(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_ge(graph::tensor<number> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			/* imm comp */
			void str_imm_lt(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			void str_imm_le(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			void str_imm_eq(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			void str_imm_ne(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			void str_imm_gt(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			void str_imm_ge(graph::tensor<number> const &x, graph::tensor<string> const &a, string const &value);

			/* string */
			void str_regex(graph::tensor<string> const &a, std::list<graph::tensor<string>> const &outputs, string const &regex);

			void str_concat(graph::tensor<string> const &x, graph::tensor<string> const &a, graph::tensor<string> const &b);

			void str_toupper(graph::tensor<string> const &x, graph::tensor<string> const &a);

			void str_tonumber(graph::tensor<number> const &x, graph::tensor<string> const &a);
		};
	}
}
