/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Dec 17, 2021
 */
#pragma once

#include "abaq/abaq.h"
#include "abaq/compute/compute.h"
#include "abaq/compute/context.h"
#include "abaq/graph/functor.h"
#include "abaq/graph/tensor.h"

#include <list>
#include <unordered_map>

namespace abaq
{
	namespace compile
	{
		class assembler;

		class unit
		{
			friend class assembler;

			compute::cseg cseg_;
			compute::aseg aseg_;
			compute::nseg nseg_;
			compute::sseg sseg_;

			std::unordered_map<graph::tensor<void>, size_t, graph::tensor<void>::hash> allocs_;
			size_t allocs_top_;

			std::list<graph::tensor<void>> vars_;

		public:
			unit() : allocs_top_(0) { }

			void build(graph::inputs<void> const &vars);

			void deploy(compute::context::ptr const &ctx);

			void anchor() { aseg_.push_back(cseg_.size()); }

			void set_index(graph::tensor<void> const &t, size_t index);

			size_t get_index(graph::tensor<void> const &t);
		};
	}
}
