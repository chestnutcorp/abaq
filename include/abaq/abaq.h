/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@chestnutcorp.com>
 * \since Jul 7, 2020
 */

#ifndef ABAQ_ABAQ
#define ABAQ_ABAQ

#include <cmath>
#include <list>
#include <string>
#include <stdexcept>
#include <string_view>

namespace abaq
{
	using number = double;
	using string = std::string;
	using size_t = std::size_t;

	const number NaN = std::nan("abaq");

	constexpr number True = -1.0;
	constexpr number False = 0.0;

	inline bool to_bool(number n) { return n != 0.0; }


	enum class value_type { number, string };

	template <typename T> value_type type_of();

	template <> inline value_type type_of<number>() { return value_type::number; }
	template <> inline value_type type_of<string>() { return value_type::string; }


	using name_list = std::list<string>;


	class type_exception : public std::invalid_argument
	{
	public:
		type_exception() : invalid_argument("Wrong tensor type.") { }
	};

	class dimension_exception : public std::invalid_argument
	{
	public:
		dimension_exception(const char *message) : invalid_argument(message) { }
	};

	class engine_exception : public std::invalid_argument
	{
	public:
		engine_exception(const std::string_view &spec) : invalid_argument("Unrecognized engine spec: " + string(spec)) { }
	};
}

#endif

